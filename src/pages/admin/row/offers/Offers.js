import React from 'react';

import {PostData} from "../../../../services/PostData";

import './Offers.css';
import FaUser from 'react-icons/lib/fa/briefcase';
import FaEdit from 'react-icons/lib/fa/edit';
import FaAdd from 'react-icons/lib/fa/plus';
import FaRemove from 'react-icons/lib/fa/trash';

export default class Offers extends React.Component {

    constructor(){
      super();

      this.state = {
        editor: false,
        add: false,
        edit: false,
        remove: false,
        title: '',
        description: '',
        company_id: '',
        type: '',
        city_id: '',
        languages: '',
        offer_edit: '',
        offers: [],
        citiesSuggest: [],
        companiesSuggest: []
      }
    }

    componentWillMount(){
        this.getOffers();
    }

    getOffers = () => {
      PostData({api: 'offers-list'}).then((result) => {
          let responseJson = result;

          if(responseJson.offersData){
              this.setState({
                offers: responseJson.offersData
              });
          }else{
              this.setState({
                offers: []
              });
          }
      });

    }

    clickAction = action => {
      switch(action){
        case 'add':
          this.setState({editor: true, add: true, edit: false, remove: false, title: '', description: '', company_id: '', type: '', city_id: '', languages: ''});
          break;
        case 'edit':
          this.setState({editor: true, add: false, edit: true, remove: false, title: '', description: '', company_id: '', type: '', city_id: '', languages: ''});
          break;
        case 'remove':
          this.setState({editor: true, add: false, edit: false, remove: true});
          break;
      }
    }
    clickOffer = (e) => {
        PostData({api: 'offers-unique', dataSearch: {id: e}}).then((result) => {
            let responseJson = result;
            if(responseJson.offersData){
                this.setState({offer_edit: responseJson.offersData, title: '', description: '', company_id: '', type: '', city_id: '', languages: '', citiesSuggest: []});
            }else{
                this.setState({offer_edit: '', usersSuggest: []});
            }
        });
    }

    cancelForm = () => {
      if(this.state.star){
        this.getStarEvents();
      }
      this.setState({editor: false, add: false, edit: false, remove: false, offer_edit: '', citiesSuggest: [], companiesSuggest: []});
    }
    onChange = (e) => {
      if(e.target.name === "city"){
          if(e.target.value.length < 3){
              this.setState({citiesSuggest: []});
          }else {
            PostData({api: 'cities-search', text: e.target.value}).then((result) => {
                let responseJson = result;
                if(responseJson.citiesData == null){
                    this.setState({citiesSuggest: []});
                }else if (responseJson.citiesData.length > 0) {
                    this.setState({citiesSuggest: responseJson.citiesData});
                }else{
                    this.setState({citiesSuggest: []});
                }
            });
          }
      }else if (e.target.name === "company") {
        if(e.target.value.length < 3){
            this.setState({companiesSuggest: []});
        }else {
          PostData({api: 'company-search', text: e.target.value}).then((result) => {
              let responseJson = result;
              if(responseJson.companyData == null){
                  this.setState({companiesSuggest: []});
              }else if (responseJson.companyData.length > 0) {
                  this.setState({companiesSuggest: responseJson.companyData});
              }else{
                  this.setState({companiesSuggest: []});
              }
          });
        }
      }else {
          switch(e.target.type){
              case 'checkbox':
                  this.setState({[e.target.value]:e.target.checked});
                  break;
              default:
                  this.setState({[e.target.name]:e.target.value});
          }
      }
    }

    render() {
      const typeCddi = (this.state.offer_edit.type === "CDD/CDI") ? "checked": "";
      const typeAlternance = (this.state.offer_edit.type === "Alternance") ? "checked": "";
      const typeStage = (this.state.offer_edit.type === "Stage") ? "checked": "";
      return (
          <div className="admin-row">
            <div className="title-icon">
              <FaUser />
            </div>
            <div className="title">
              Offres
            </div>
            <div className="actions">
              <div className="action-item add" onClick={() => this.clickAction("add")}><FaAdd /></div>
              <div className="action-item edit" onClick={() => this.clickAction("edit")}><FaEdit /></div>
              <div className="action-item remove" onClick={() => this.clickAction("remove")}><FaRemove /></div>
            </div>
            { this.state.editor &&
              <div className="editor-container offers-editor-container">
              { this.state.add &&
                <div className="event-editor-edit-form">
                  <div className="events-editor-add-block">

                      <div className="field">
                          <input id="admin-offers-edit-title" type="text" name="title" placeholder=" "  autoComplete="off" defaultValue={this.state.offer_edit.title} onChange={this.onChange}/>
                          <label htmlFor="admin-offers-edit-title">Titre</label>
                      </div>

                      <div className="field">
                          <input id="admin-offers-edit-company" type="text" name="company" placeholder=" " ref='city' autoComplete="off" defaultValue={this.state.offer_edit.company_name} onChange={this.onChange}/>
                          <label htmlFor="admin-offers-edit-company">Entreprise</label>
                          <div className="cities-suggest">
                              {
                                  this.state.companiesSuggest.map(el => <span onClick={this.clickCompany} id-city={el.company_id} key={el.company_id}> {el.company_name } </span>)
                              }
                          </div>
                      </div>

                      <div className="field">
                          <input id="user-edit-city" type="text" name="city" placeholder=" " ref='city' defaultValue={this.state.offer_edit.nom_reel} autoComplete="off" onChange={this.onChange}/>
                          <label htmlFor="user-edit-city">Ville</label>
                          <div className="cities-suggest">
                              {
                                  this.state.citiesSuggest.map(el => <span onClick={this.clickCity} id-city={el.id} key={el.id}> {el.nom_reel + ' (' + el.departement + ')' } </span>)
                              }
                          </div>
                      </div>

                      <div className="checkbox-group">
                          <div className="checkbox">
                              <input id="offer-type-cddi" type="checkbox" name="type" value="cddi"  onChange={this.onChange}/>
                              <label htmlFor="offer-type-cddi">CDI / CDD</label>
                          </div>
                          <div className="checkbox">
                              <input id="offer-type-alt" type="checkbox" name="type" value="alternance" onChange={this.onChange}/>
                              <label htmlFor="offer-type-alt">Alternance</label>
                          </div>
                          <div className="checkbox">
                              <input id="offer-type-stage" type="checkbox" name="type" value="stage" onChange={this.onChange}/>
                              <label htmlFor="offer-type-stage">Stage</label>
                          </div>
                      </div>

                      <div className="field">
                          <input id="admin-offers-edit-languages" type="text" name="languages" placeholder=" "  autoComplete="off" defaultValue={this.state.offer_edit.languages} onChange={this.onChange}/>
                          <label htmlFor="admin-offers-edit-languages">Langages (séparation par virgule)</label>
                      </div>
                  </div>

                  <div className="events-editor-add-block">
                      <label htmlFor="admin-offers-edit-description">Décrivez l&apos;offre</label>
                      <textarea id="admin-offers-edit-description" name="message" defaultValue={this.state.offer_edit.description} onChange={this.onChange} />

                      <div className="button margin" onClick={() => this.editOffer()}>Modifier l&apos;évènement</div>
                      <div className="button grey" onClick={() => this.cancelForm()}>Annuler</div>
                  </div>
                </div>
              }
              { this.state.edit &&
                <div className="events-editor-edit">
                  { !this.state.offer_edit &&
                    <div className="events-editor-add-block events-list events-list-edit">
                        <span className="events-editor-star-title">
                          Choisissez premièrement l&apos;offre que vous souhaitez modifier.
                        </span>
                        <span className="button grey" onClick={() => this.cancelForm()}>Annuler</span>
                        {
                            this.state.offers.map(el => <div onClick={() => this.clickOffer(el.id)} key={'events-star-' + el.id}><div>{el.title}</div><div>Offre de {el.company_name}</div> </div> )
                        }
                    </div>
                  }
                  { this.state.offer_edit &&
                    <div className="event-editor-edit-form">
                      <div className="events-editor-add-block">

                          <div className="field">
                              <input id="admin-offers-edit-title" type="text" name="title" placeholder=" "  autoComplete="off" defaultValue={this.state.offer_edit.title} onChange={this.onChange}/>
                              <label htmlFor="admin-offers-edit-title">Titre</label>
                          </div>

                          <div className="field">
                              <input id="admin-offers-edit-company" type="text" name="company" placeholder=" " ref='city' autoComplete="off" defaultValue={this.state.offer_edit.company_name} onChange={this.onChange}/>
                              <label htmlFor="admin-offers-edit-company">Entreprise</label>
                              <div className="cities-suggest">
                                  {
                                      this.state.companiesSuggest.map(el => <span onClick={this.clickCompany} id-city={el.company_id} key={el.company_id}> {el.company_name } </span>)
                                  }
                              </div>
                          </div>

                          <div className="field">
                              <input id="user-edit-city" type="text" name="city" placeholder=" " ref='city' defaultValue={this.state.offer_edit.nom_reel} autoComplete="off" onChange={this.onChange}/>
                              <label htmlFor="user-edit-city">Ville</label>
                              <div className="cities-suggest">
                                  {
                                      this.state.citiesSuggest.map(el => <span onClick={this.clickCity} id-city={el.id} key={el.id}> {el.nom_reel + ' (' + el.departement + ')' } </span>)
                                  }
                              </div>
                          </div>

                          <div className="checkbox-group">
                              <div className="checkbox">
                                  <input id="offer-type-cddi" type="checkbox" name="type" value="cddi" {...typeCddi} onChange={this.onChange}/>
                                  <label htmlFor="offer-type-cddi">CDI / CDD</label>
                              </div>
                              <div className="checkbox">
                                  <input id="offer-type-alt" type="checkbox" name="type" value="alternance" {...typeAlternance}  onChange={this.onChange}/>
                                  <label htmlFor="offer-type-alt">Alternance</label>
                              </div>
                              <div className="checkbox">
                                  <input id="offer-type-stage" type="checkbox" name="type" value="stage" {...typeStage} onChange={this.onChange}/>
                                  <label htmlFor="offer-type-stage">Stage</label>
                              </div>
                          </div>

                          <div className="field">
                              <input id="admin-offers-edit-languages" type="text" name="languages" placeholder=" "  autoComplete="off" defaultValue={this.state.offer_edit.languages} onChange={this.onChange}/>
                              <label htmlFor="admin-offers-edit-languages">Langages (séparation par virgule)</label>
                          </div>

                      </div>
                      <div className="events-editor-add-block">
                          <label htmlFor="admin-offers-edit-description">Décrivez l&apos;offre</label>
                          <textarea id="admin-offers-edit-description" name="message" defaultValue={this.state.offer_edit.description} onChange={this.onChange} />

                          <div className="button margin" onClick={() => this.editOffer()}>Modifier l&apos;évènement</div>
                          <div className="button grey" onClick={() => this.cancelForm()}>Annuler</div>
                      </div>
                    </div>
                  }
                </div>
              }
              </div>
            }
          </div>
      );
    }
}
