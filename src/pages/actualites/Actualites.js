import React, {Component} from 'react';


import BlogPost from "./row/BlogPost";
import BlogPage from "./row/BlogPage";

export default class Actualites extends Component {

  render() {
    const queryString = require('query-string');
    var parsed = queryString.parse(this.props.location.search);

    if(parsed.post_id){
      return (
        <div className="Actualites">
          <BlogPost post_id={parsed.post_id}/>
        </div>
      );
    }
    else {
      return (
        <div className="Actualites">
          <BlogPage category_id={-1}/>
        </div>
    );}
  }

}
