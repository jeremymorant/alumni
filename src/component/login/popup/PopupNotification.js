import React, {Component} from 'react';

import './PopupNotification.css';
import {PostData} from "../../../services/PostData";

import FaCross from 'react-icons/lib/fa/times-circle';

export default class popupOfferDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            ready: false,
            description: '',
            title: '',
            color: ''
        };
        this.hideNotification = this.hideNotification.bind(this);
        this.showNotification = this.showNotification.bind(this);
    }

    hideNotification = () => {
        this.setState({ready: false});
    }

    showNotification = () => {
        this.setState({ready: true});

        setTimeout(() => {
          this.hideNotification()
        }, 5000);
    }

    render() {

        return (
                <div className={this.state.ready ? 'popup-notification ' + this.props.color : 'popup-notification hidden ' + this.props.color}>
                    <span className="close-notification" onClick={this.hideNotification}>
                      <FaCross />
                    </span>
                    <span className="notification-title">
                        {this.props.title}
                    </span>
                    <span className="notification-description">
                        {this.props.description}
                    </span>
                </div>
        );
    }

}
