import React, {Component} from 'react';

import { PostData } from "../../services/PostData";

import Calendar from "./row/Calendar";
import Slider from "./slider/Slider";
import PopupEventDetails from "./popup/PopupEventDetails";

export default class Agenda extends Component {

    constructor(){
      super();

      let today = new Date();
      let yearActual = today.getFullYear();
      let monthActual = today.getMonth() + 1;

      this.state = {
          ready: false,
          calendarEvents: [],
          calendarMessages: [],
          culture: 'fr',
          year: yearActual,
          month: monthActual,
          eventDatas: {
            title: '',
            type: '',
            startDate: '',
            endDate: '',
            nom_reel: '',
            address: '',
            description: ''
          }
      }
    }

    componentDidMount(){

        this.getEvents();

        setTimeout(() => {
          window.dispatchEvent(new Event('resize'));
        }, 100);
  }

  getEvents(){
    PostData({api: 'agenda-month', dataAgenda: {year: this.state.year, month: this.state.month}}).then((result) => {
        let responseJson = result;

        if (responseJson.agendaData) {
            var arr = [];
            Object.keys(responseJson.agendaData).forEach(function(key) {
              arr.push(responseJson.agendaData[key]);
              arr[key].startDate = new Date(arr[key].startDate);
              arr[key].endDate = new Date(arr[key].endDate);
            });
            this.setState({calendarEvents: arr, ready: true});
        }else{
            this.setState({calendarEvents: [], ready: true});
        }
    });
  }


  dateChangedCallBack = (direction) => {
    let month = 0;
    let year = 0;
    if(direction === "moins"){
        if(this.state.month == 1){
          month = 12;
          year = this.state.year - 1;
        }else{
          month = this.state.month - 1;
          year = this.state.year;
        }
    }else if(direction === "plus"){
        if(this.state.month == 12){
          month = 1;
          year = this.state.year + 1;
        }else{
          month = this.state.month + 1;
          year = this.state.year;
        }
    }else{
        let d = new Date();
        month = d.getMonth();
        year = d.getYear();
    }

    PostData({api: 'agenda-month', dataAgenda : {year: year, month: month}}).then((result) => {
        let responseJson = result;
        if (responseJson.agendaData) {
            this.setState({
                events: responseJson.agendaData,
                year: year,
                month: month
            });
        }
    });
  };

  dateClickCallBack = (id) => {
      PostData({api: 'agenda-unique', dataAgenda : {id: id}}).then((result) => {
          let responseJson = result;
          if (responseJson.agendaData) {
              this.setState({
                  eventDatas: responseJson.agendaData
              });
          }
          this.popupChild.showPopup();
      });
  }


  render() {
      return (
        <div>
            <Slider />
            <div className="Agenda">
                { this.state.ready &&
                  <Calendar events={this.state.calendarEvents} dateChangedCallBack={this.dateChangedCallBack} dateClickCallBack={this.dateClickCallBack}/>
                }
            </div>
            <PopupEventDetails ref={instance => { this.popupChild = instance; }} eventDatas={this.state.eventDatas}/>
        </div>
    );
  }

}
