import React from 'react';

import './BlogPage.css';
import {PostData} from "../../../services/PostData";

export default class BlogPage extends React.Component {
  constructor(){
      super();
      this.state = { api: 'blog-page', posts: [], categories: [], nbOfPosts: '', postIncrement: '', searchTerm: '', redirectToReferrer: false};
      this.getPosts = this.getPosts.bind(this);
      this.getFirstParagraph = this.getFirstParagraph.bind(this);
      this.loadMorePosts = this.loadMorePosts.bind(this);
      this.componentDidMount = this.componentDidMount.bind(this);
      this.addOrRemoveFilter = this.addOrRemoveFilter.bind(this);
      this.search = this.search.bind(this);
      this.nbOfPosts = 0;
      this.postIncrement = 2;
  }

  getPosts() {
    this.setState({postIncrement: this.postIncrement, nbOfPosts: this.nbOfPosts }, () => {
      PostData(this.state).then((result) => {
          let responseJson = result;
          if(responseJson.postsDetails){
            this.setState({
              posts: responseJson.postsDetails,
              redirectToReferrer: true});
            this.nbOfPosts += this.postIncrement
          }
      });
    });
  }

  getFirstParagraph(str) {
    return str.substring(0, str.indexOf("</p>") + 4);
  }

  loadMorePosts() {
    this.setState({postIncrement: this.postIncrement, nbOfPosts: this.nbOfPosts }, () => {
      PostData(this.state).then((result) => {
          let responseJson = result;
          if(responseJson.postsDetails){
              this.setState({
                posts: this.state.posts.slice().concat(responseJson.postsDetails),
                redirectToReferrer: true});
              this.nbOfPosts += this.postIncrement
          }
      });
    });
  }

  addOrRemoveFilter(category_id) {
    console.log(this.state);
    if (this.state.categories.includes(category_id)) {
      let id = this.state.categories.indexOf(category_id);
      let newCategories = [];
      if(this.state.categories.length > 1) {
       newCategories = [...this.state.categories].splice(id, 1);
      }
      this.setState({categories: newCategories, posts: []}, () => {
        this.nbOfPosts = 0;
      });
    }
    else{
      this.setState({categories:[...this.state.categories, category_id], posts: []}, () => {
        this.nbOfPosts = 0;
      });
    }
      console.log(this.state);
  }

  search(searchTerm) {
    this.setState({searchTerm: searchTerm, posts: []}, () => {
      this.nbOfPosts = 0;
    });
  }

  componentDidMount(){
      this.getPosts();
      this.refs.iScroll.addEventListener("scroll", () => {
      if (this.refs.iScroll.scrollTop + this.refs.iScroll.clientHeight >=this.refs.iScroll.scrollHeight){
        this.loadMorePosts();
      }
    });
  }

  render() {
    return (
      <div className="container">

        <div className="row parent">
          <div className="col-sm-8 contpost" ref="iScroll">
            <div className="main-title-container">
                <h2>LES NEWS D&apos;UN MIAGISTE</h2>
                <h3>Restez informés des différents évènements à venir, afterwork, rencontres avec les entreprises, évènements informatiques et d&apos;autres encore. </h3>
            </div>
        {
          this.state.posts.map(el =>
              <div key={'blog-post-id-'+el.id}>
                <br/>
                <div className="row">
                  <div className="col PostTitle">
                    <h4><a href={'/actualites?post_id='+el.id} key={'blog-post-title-'+el.id}>{el.title}</a></h4>
                    <div className="PostDate" key={'blog-post-date-'+el.id}><i className="fa fa-calendar"></i>
                      {" " + el.date + " | Catégorie : "}<a href="#"> <span className={el.category_style}  onClick={(e) => this.addOrRemoveFilter(el.category_id)}>{el.category_name}</span></a>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12">
                    <a href={'/actualites?post_id='+el.id} className="thumbnail">
                        <img src={el.image} alt="" className="blogImg" key={'blog-post-image-'+el.id}/>
                    </a>
                  </div>
                  <div className="col-sm-12">
                    <div dangerouslySetInnerHTML={{ __html: this.getFirstParagraph(el.content)}} key={'blog-post-first-paragraph-'+el.id}></div>
                    <p><a className="btn btn-sm btn-success" href={'/actualites?post_id='+el.id}>En savoir +</a></p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12">
                    <p>
                      <i className="fa fa-user-circle"></i> Par <a href={'/annuaire?user_id='+el.author_id} key={'blog-post-author-'+el.id}>{el.author_name} </a>
                        <i className="fa fa-comment"></i> <a href={'/actualites?post_id='+el.id}>{el.comments} Commentaires </a>
                        <i className="fa fa-share"></i> <a href={'/actualites?post_id='+el.id}>{el.likes} J aime</a>
                    </p>
                  </div>
                </div>
                <hr/>
              </div>
          )
        }
      </div>


        <div className="col-md-4 mobile-shadow-top">

          {/*Search Widget*/}
          <div className="card my-4">
            <h5 className="card-header">Recherche</h5>
            <div className="card-body">
              {
                this.state.categories.map(el =>
                  <span key={'category-span-id-'+el}><i className={'fas fa-times remove'} onClick={(e) => this.addOrRemoveFilter(el)}></i></span>
                )
              }
              <div className="input-group">
                <input type="text" className="form-control" id="postSearch" placeholder="Rechercher..."/>
                <span className="input-group-btn">
                  <button className="btn btn-secondary" onClick={(e) => this.search(document.getElementById('postSearch').value)} type="button">Go!</button>
                </span>
              </div>
            </div>
          </div>

          {/*Categories Widget*/}
          <div className="card my-4">
            <h5 className="card-header">Categories</h5>
            <div className="card-body">
              <div className="row">
                <div className="col-lg-6">
                  <ul className="list-unstyled mb-0">
                    <li>
                      <a href="#" onClick={(e) => this.addOrRemoveFilter(1)}>Afterwork</a>
                    </li>
                    <li>
                      <a href="#" onClick={(e) => this.addOrRemoveFilter(2)}>Gala</a>
                    </li>
                    <li>
                      <a href="#" onClick={(e) => this.addOrRemoveFilter(3)}>MIAGE</a>
                    </li>
                  </ul>
                </div>
                <div className="col-lg-6">
                  <ul className="list-unstyled mb-0">
                    <li>
                      <a href="#" onClick={(e) => this.addOrRemoveFilter(4)}>Informatique</a>
                    </li>
                    <li>
                      <a href="#" onClick={(e) => this.addOrRemoveFilter(5)}>Gestion</a>
                    </li>
                    <li>
                      <a href="#" onClick={(e) => this.addOrRemoveFilter(6)}>Familles</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          {/*Side Widget*/}
          <div className="card my-4">
            <h5 className="card-header">Twitter</h5>
            <div className="card-body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi.
            </div>
          </div>

        </div>
        </div>
        </div>
    );
  }
}
