import React from 'react';

import './RowSearch.css';
import {PostData} from "../../../services/PostData";

export default class RowSearch extends React.Component {
    constructor(){
        super();
        this.state = {
            cddi: false,
            alternance: false,
            stage: false,
            city: '',
            keywords: '',
            citiesSuggest: [],
            page: 1,
            pages: 1,
            redirectToReferrer: false
        };

        this.search = this.search.bind(this);
        this.onChange = this.onChange.bind(this);
        this.clickCity = this.clickCity.bind(this);

    }

    clickCity(e){
        this.refs.city.value = e.target.innerHTML;
        this.setState({
          city: e.target.getAttribute("id-city"),
          citiesSuggest: []
        });
    }

    onChange(e){
        if(e.target.name === "city"){
            if(e.target.value.length < 3){
                this.setState({citiesSuggest: []});
            }else {
                PostData({api: 'cities-search', text: e.target.value}).then((result) => {
                    let responseJson = result;
                    if(responseJson.citiesData == null){
                        this.setState({citiesSuggest: []});
                    }else if (responseJson.citiesData.length > 0) {
                        this.setState({citiesSuggest: responseJson.citiesData});
                    }else{
                        this.setState({citiesSuggest: []});
                    }
                });
            }
            switch(e.target.type){
                case 'checkbox':
                    this.setState({[e.target.value]:e.target.checked});
                    break;
                default:
                    this.setState({[e.target.name]:e.target.value});
            }
        }else {
            switch(e.target.type){
                case 'checkbox':
                    this.setState({[e.target.value]:e.target.checked});
                    break;
                default:
                    this.setState({[e.target.name]:e.target.value});
            }
        }
    }

    search(){
        if(this.state.cddi || this.state.alternance || this.state.stage || this.state.city || this.state.keywords){
            this.setState({page: 1});

            let datas = {
                cddi: this.state.cddi,
                alternance: this.state.alternance,
                stage: this.state.stage,
                city: this.state.city,
                keywords: this.state.keywords,
                page: 1
            };

            this.props.researchCallBack(datas);
        }
    }

    render() {
        return (
            <div className="offers-container">
                <div className="search-bar">
                    <div className="search-bar-title">
                        <h1>Recherche d&apos;offres</h1>
                        <p>Vous êtes à la recherche d'un stage, d'une alternance ou d&apos;un emploi ? Consultez dés à présent les offres de nos nombreux partenaires dans le domaine de l&apos;informatique.</p>
                    </div>
                    <div className="search-bar-item">
                        <p>Type d&apos;offre</p>
                        <div className="checkbox-group">
                            <div className="checkbox">
                                <input id="offer-type-cddi" type="checkbox" name="type" value="cddi" onChange={this.onChange}/>
                                <label htmlFor="offer-type-cddi">CDI / CDD</label>
                            </div>
                            <div className="checkbox">
                                <input id="offer-type-alt" type="checkbox" name="type" value="alternance" onChange={this.onChange}/>
                                <label htmlFor="offer-type-alt">Alternance</label>
                            </div>
                            <div className="checkbox">
                                <input id="offer-type-stage" type="checkbox" name="type" value="stage" onChange={this.onChange}/>
                                <label htmlFor="offer-type-stage">Stage</label>
                            </div>
                        </div>
                    </div>
                    <div className="search-bar-item">
                        <label htmlFor="offer-city">Lieu</label>
                        <input id="offer-city" type="text" name="city" autoComplete="off" ref="city" placeholder="Ville, département" onChange={this.onChange}/>
                        <div id="cities-suggest">
                            {
                                this.state.citiesSuggest.map(el => <span onClick={this.clickCity} id-city={el.id} key={el.id}> {el.nom_reel + ' (' + el.departement + ')' } </span>)
                            }
                        </div>
                    </div>
                    <div className="search-bar-item search-bar-item-long">
                        <label htmlFor="offer-kwordsey">Langages</label>
                        <input id="offer-keywords" type="text" name="keywords" autoComplete="off" placeholder="JavaScript, Oracle, Management, etc." onChange={this.onChange}/>
                    </div>
                    <div className="search-bar-item search-bar-submit">
                        <input type="submit" value="Rechercher" onClick={this.search}/>
                    </div>
                </div>
            </div>
        )
    }
}
