import React from 'react';

export default class UserSearch extends React.Component {
  constructor(){
      super();
      this.state = { api: 'users-search', users: [], redirectToReferrer: false};
      this.getUsers = this.getUsers.bind(this);
      this.componentDidMount = this.componentDidMount.bind(this);
  }

  search(searchTerm) {
    this.setState({searchTerm: searchTerm, posts: []}, () => {
      this.nbOfPosts = 0;
      console.log(this.state);
    });
  }

  render() {
    return (
      <div className="container">
        <br/>
        <div class="row">
          <div class="col-lg-6">

          </div>
          <div class="col-lg-6 float-right">
            <div className="input-group">
              <label for="userSearch">Rechercher des utilisateurs :</label>
              <input type="text" className="form-control" id="userSearch" placeholder="Rechercher..."/>
              <span className="input-group-btn">
                <button className="btn btn-secondary" onClick={(e) => this.search(document.getElementById('postSearch').value)} type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
