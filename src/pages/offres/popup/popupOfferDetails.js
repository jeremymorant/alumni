import React, {Component} from 'react';

import './popupOfferDetails.css';
import {PostData} from "../../../services/PostData";

import FaCross from 'react-icons/lib/fa/times-circle';

export default class popupOfferDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            ready: false,
            form: false,
            message: ''
        };
        this.hidePopup = this.hidePopup.bind(this);
        this.showPopup = this.showPopup.bind(this);
        this.cancelForm = this.cancelForm.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.showForm = this.showForm.bind(this);
        this.onChange = this.onChange.bind(this);
    }


    hidePopup(){
        console.log("hidePopup");
        this.setState({ready: false, form: false});
        //clear fields
    }

    showPopup(){
        console.log("showPopup");
        this.setState({ready: true});
    }

    showForm(){
        console.log("showForm");
        this.setState({form: true});
    }

    cancelForm(){
      console.log("cancelForm");
      //
      //
      //
      this.setState({form: false});
    }

    onChange(e){
          this.setState({[e.target.name]:e.target.value});
          console.log(this.state);
    }

    render() {

        return (
                <div className={this.state.ready ? 'popup' : 'popup hidden'}>
                    <div className="popup-overlay" onClick={this.hidePopup}> </div>
                    <div className="popup-container">
                        { !this.state.form &&
                            <div className="popup-details">
                                <div className="popup-header">
                                    <span className="popup-header-title">Détails de l&apos;offre</span>
                                    <FaCross className="popup-cross" onClick={this.hidePopup}/>
                                </div>
                                <div className="popup-body">
                                    <div className="popup-body-title">
                                        {this.props.offerDetails.offer_title}
                                    </div>
                                    <div className="popup-body-company">
                                        {this.props.offerDetails.offer_company}&nbsp;
                                    </div>
                                    <div className="popup-body-city">
                                        - {this.props.offerDetails.offer_city} ({this.props.offerDetails.offer_cp})
                                    </div>
                                    <div className="popup-body-type">
                                        {this.props.offerDetails.offer_type}
                                    </div>
                                    <div className="popup-body-description">
                                        {this.props.offerDetails.offer_description}
                                    </div>
                                </div>
                                <div className="popup-footer">
                                    <div className="popup-footer-button" onClick={this.showForm}>Déposer ma candidature</div>
                                </div>
                            </div>
                        }
                        { this.state.form &&
                          <div className="popup-form">
                              <div className="popup-details">
                                  <div className="popup-header">
                                      <span className="popup-header-title">Détails de l&apos;offre</span>
                                      <FaCross className="popup-cross" onClick={this.hidePopup}/>
                                  </div>
                                  <div className="popup-body">
                                        <div>{this.props.userDetails.user_name}</div>
                                        <div>{this.props.userDetails.user_promotion + " " + this.props.universityDetails.university_name}</div>
                                        <div>{this.props.universityDetails.university_city}</div>
                                        <div>{this.props.offerDetails.offer_type+" - "+this.props.offerDetails.offer_title}</div>
                                        <textarea id="apply-message" name="message" placeholder=" " onChange={this.onChange}/>
                                  </div>
                                  <div className="popup-footer">
                                      <div className="popup-footer-button" onClick={this.hidePopup}>Annuler</div>
                                      <div className="popup-footer-button" onClick={this.validateForm}>Envoyer</div>
                                  </div>
                              </div>
                          </div>
                        }
                    </div>
                </div>
        );
    }

    validateForm = () => {
      if(this.state.message.length > 0){

        let datas = {
            user_id: this.props.userDetails.user_id,
            user_name: this.props.userDetails.user_name,
            university_name: this.props.universityDetails.university_name,
            university_city: this.props.universityDetails.university_city,
            user_promotion: this.props.userDetails.user_promotion,
            offer_id: this.props.offerDetails.offer_id,
            offer_type: this.props.offerDetails.offer_type,
            offer_title: this.props.offerDetails.offer_title,
            email_message: this.state.message
        }
        PostData({api: 'offer-apply', dataApply: datas}).then((result) => {
            let responseJson = result;

            if (responseJson.success) {
                this.props.applySentCallback(true);
            } else {
                this.props.applySentCallback(false);
            }

            this.hidePopup();
        });
      }
    }
}
