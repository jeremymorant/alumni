import React, {Component} from 'react';

import {PostData} from "../../services/PostData";

import Users from "./row/users/Users";
import Companies from "./row/companies/Companies";
import Events from "./row/events/Events";
import News from "./row/news/News.js";
import Offers from "./row/offers/Offers.js";

export default class Compte extends Component {

  constructor(){
      super();

      this.state = {

      };
  }

  render() {
    return (
      <div className="Admin">
        <div className="Account-inner">
          <div className="Account-title">
            <h1>Espace Administrateur</h1>
            <p>Vous pourrez ici modifier les différents Carousel, ajouter, supprimer et modifier des utilisateurs ou entreprises. Vous pourrez aussi editer des offres, des actualités ou des évenements.</p>
          </div>
          <div className="Admin-inner">
            <Users />
            <Companies />
            <News />
            <Offers />
            <Events />
          </div>
        </div>
      </div>
    )
  }

}
