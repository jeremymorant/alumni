import React from 'react';

import FaSuitcase from 'react-icons/lib/fa/suitcase';
import FaCalendar from 'react-icons/lib/fa/calendar';
import FaNewspaper from 'react-icons/lib/fa/file';
import FaPeople from 'react-icons/lib/fa/user';

import './RowStats.css';
import {PostData} from "../../../services/PostData";

import Offres from './img/offres.jpg';
import Events from './img/events.jpg';
import News from './img/news.jpg';
import Members from './img/members.jpg';

export default class extends React.Component {
  constructor(){
      super();
      this.state = { api: 'home-stats', stats: [], redirectToReferrer: false};
      this.getStats = this.getStats.bind(this);
      this.componentDidMount = this.componentDidMount.bind(this);
  }

  getStats() {
    PostData(this.state).then((result) => {
        let responseJson = result;
        if(responseJson.stats){
            this.setState({
              stats: responseJson.stats,
              redirectToReferrer: true});
        }
    });
  }

  componentDidMount(){
      this.getStats();
  }

    render() {
        return (
            <div className="RowStats-container">

                <div className="RowStats-item" style={ { backgroundImage: "url("+Offres+")" } }>
                    <div className="RowStats-banner">
                        <div className="RowStats-banner-icon">
                            <FaSuitcase />
                        </div>
                        <div className="RowStats-banner-number">
                            {this.state.stats.offersCount}
                        </div>
                        <div className="RowStats-banner-label">
                            offres
                        </div>
                    </div>
                </div>

                <div className="RowStats-item" style={ { backgroundImage: "url("+Events+")" } }>
                    <div className="RowStats-banner">
                        <div className="RowStats-banner-icon">
                            <FaCalendar />
                        </div>
                        <div className="RowStats-banner-number">
                            {this.state.stats.eventsCount}
                        </div>
                        <div className="RowStats-banner-label">
                            événements
                        </div>
                    </div>
                </div>

                <div className="RowStats-item" style={ { backgroundImage: "url("+News+")" } }>
                    <div className="RowStats-banner">
                        <div className="RowStats-banner-icon">
                            <FaNewspaper />
                        </div>
                        <div className="RowStats-banner-number">
                            {this.state.stats.articlesCount}
                        </div>
                        <div className="RowStats-banner-label">
                            actualités
                        </div>
                    </div>
                </div>

                <div className="RowStats-item" style={ { backgroundImage: "url("+Members+")" } }>

                    <div className="RowStats-banner">
                        <div className="RowStats-banner-icon">
                            <FaPeople />
                        </div>
                        <div className="RowStats-banner-number">
                            {this.state.stats.usersCount}
                        </div>
                        <div className="RowStats-banner-label">
                            membres
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
