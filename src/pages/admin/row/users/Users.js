import React from 'react';

import {PostData} from "../../../../services/PostData";


import './Users.css';
import FaUser from 'react-icons/lib/fa/user';
import FaEdit from 'react-icons/lib/fa/edit';
import FaAdd from 'react-icons/lib/fa/plus';
import FaRemove from 'react-icons/lib/fa/trash';

export default class Users extends React.Component {
  constructor(){
    super();

    this.state = {
      citiesSuggest: [],
      usersSuggest: [],
      user_edit: '',
      editor: false,
      edit: false,
      remove: false,
      nom: '',
      prenom: '',
      naissance: '',
      email: '',
      promotion: '',
      statut: '',
      city_id: ''
    }
  }

  clickAction = action => {
    switch(action){
      case 'add':
        this.setState({editor: true, star: false, add: true, edit: false, remove: false, nom: '', statut: '', prenom: '', city_id: '', naissance: '', promotion: '', email: ''});
        break;
      case 'edit':
        this.setState({editor: true, star: false, add: false, edit: true, remove: false, nom: '', statut: '', prenom: '', city_id: '', naissance: '', promotion: '', email: ''});
        break;
      case 'remove':
        this.setState({editor: true, star: false, add: false, edit: false, remove: true});
        break;
    }
  }
  clickCity = (e) => {
      this.refs.city.value = e.target.innerHTML;
      this.setState({
        city_id: e.target.getAttribute("id-city"),
        citiesSuggest: []
      });
  }
  clickUser = (e) => {
      let user_id = e.target.getAttribute("id-city");
      PostData({api: 'user-unique', dataUser: {id: user_id}}).then((result) => {
          let responseJson = result;
          if(responseJson.userData){
              this.setState({user_edit: responseJson.userData,  nom: '', statut: '', prenom: '', city_id: '', naissance: '', promotion: '', email: '', usersSuggest: []});
          }else{
              this.setState({user_edit: '', usersSuggest: []});
          }
      });
  }

  cancelForm = () => {
    if(this.state.star){
      this.getStarEvents();
    }
    this.setState({editor: false, add: false, edit: false, remove: false, user_edit: '',});
  }
  onChange = (e) => {
    if(e.target.name === "city"){
        if(e.target.value.length < 3){
            this.setState({citiesSuggest: []});
        }else {
          PostData({api: 'cities-search', text: e.target.value}).then((result) => {
              let responseJson = result;
              if(responseJson.citiesData == null){
                  this.setState({citiesSuggest: []});
              }else if (responseJson.citiesData.length > 0) {
                  this.setState({citiesSuggest: responseJson.citiesData});
              }else{
                  this.setState({citiesSuggest: []});
              }
          });
        }
    }else if (e.target.name === "user_search") {
      if(e.target.value.length < 3){
          this.setState({usersSuggest: []});
      }else {
        PostData({api: 'user-search', text: e.target.value}).then((result) => {
            let responseJson = result;
            if(responseJson.userData == null){
                this.setState({usersSuggest: []});
            }else if (responseJson.userData.length > 0) {
                this.setState({usersSuggest: responseJson.userData});
            }else{
                this.setState({usersSuggest: []});
            }
        });
      }
    }else {
        switch(e.target.type){
            case 'checkbox':
                this.setState({[e.target.value]:e.target.checked});
                break;
            default:
                this.setState({[e.target.name]:e.target.value});
        }
    }
  }


  addUser = () => {
    PostData({api: 'user-add', dataUser : this.state}).then((result) => {
      let responseJson = result;

      this.setState({
        editor: false,
        add: false,
        user_edit: ''
      });
    });
  }
  editUser = () => {
    PostData({api: 'user-edit', dataUser : this.state}).then((result) => {
      let responseJson = result;

      this.setState({
        editor: false,
        edit: false,
        user_edit: ''
      });
    });
  }

  render() {
    return (
        <div className="admin-row">
          <div className="title-icon">
            <FaUser />
          </div>
          <div className="title">
            Utilisateurs
          </div>
          <div className="actions">
            <div className="action-item add" onClick={() => this.clickAction("add")}><FaAdd /></div>
            <div className="action-item edit" onClick={() => this.clickAction("edit")}><FaEdit /></div>
            <div className="action-item remove" onClick={() => this.clickAction("remove")}><FaRemove /></div>
          </div>

          { this.state.editor &&
            <div className="editor-container users-editor-container">
            { this.state.add &&
              <div className="event-editor-add">
                <div className="field field-half">
                    <input id="user-edit-lastname" type="text" name="nom" placeholder=" " defaultValue={this.state.user_edit.nom} autoComplete="off" onChange={this.onChange}/>
                    <label htmlFor="user-edit-lastname">Nom</label>
                </div>
                <div className="field field-half">
                    <input id="user-edit-firstname" type="text" name="prenom" placeholder=" " defaultValue={this.state.user_edit.prenom} autoComplete="off" onChange={this.onChange}/>
                    <label htmlFor="user-edit-firstname">Prénom</label>
                </div>
                <div className="field field-half">
                    <input id="user-edit-birth" type="date" name="naissance" placeholder=" " defaultValue={this.state.user_edit.naissance} autoComplete="off" onChange={this.onChange}/>
                    <label htmlFor="user-edit-birth">Date de Naissance</label>
                </div>
                <div className="field field-half">
                    <input id="user-edit-email" type="email" name="email" placeholder=" " defaultValue={this.state.user_edit.email} autoComplete="off"  onChange={this.onChange}/>
                    <label htmlFor="user-edit-email">Adresse mail</label>
                </div>
                <div className="field">
                    <input id="user-edit-city" type="text" name="city" placeholder=" " ref='city' defaultValue={this.state.user_edit.nom_reel} autoComplete="off" onChange={this.onChange}/>
                    <label htmlFor="user-edit-city">Ville</label>
                    <div className="cities-suggest">
                        {
                            this.state.citiesSuggest.map(el => <span onClick={this.clickCity} id-city={el.id} key={el.id}> {el.nom_reel + ' (' + el.departement + ')' } </span>)
                        }
                    </div>
                </div>
                <div className="field field-half">
                    <input id="user-edit-promotion" type="text" name="promotion" placeholder=" " defaultValue={this.state.user_edit.promotion} autoComplete="off" onChange={this.onChange}/>
                    <label htmlFor="user-edit-promotion">Promotion</label>
                </div>
                <div className="field field-half">
                    <input id="user-edit-statut" type="text" name="statut" placeholder=" " defaultValue={this.state.user_edit.statut} autoComplete="off"  onChange={this.onChange}/>
                    <label htmlFor="user-edit-statut">Statut</label>
                </div>

                  <div className="button grey margin" onClick={() => this.cancelForm()}>Annuler</div>
                  <div className="button " onClick={() => this.addUser()}>Valider</div>
              </div>
            }
            { this.state.edit &&
              <div className="event-editor-edit">
                { !this.state.user_edit &&
                  <div className="event-editor-add">

                    <div className="users-editor-edit-title">
                      Commencez par rechercher la personne dont vous voulez modifier les informations.
                    </div>
                    <div className="field search">
                        <input id="user-edit-search" type="text" name="user_search" placeholder=" " ref='user_search' defaultValue={this.state.user_edit.nom_reel} autoComplete="off" onChange={this.onChange}/>
                        <label htmlFor="user-edit-search">Recherche</label>
                        <div className="cities-suggest">
                            {
                                this.state.usersSuggest.map(el => <span onClick={this.clickUser} id-city={el.id} key={el.id}> {el.nom + ' ' + el.prenom } </span>)
                            }
                        </div>
                    </div>
                    <div className="button grey search" onClick={() => this.cancelForm()}>Annuler</div>
                  </div>
                }
                { this.state.user_edit &&
                  <div className="event-editor-add">
                    <div className="field field-half">
                        <input id="user-edit-lastname" type="text" name="nom" placeholder=" " defaultValue={this.state.user_edit.nom} autoComplete="off" onChange={this.onChange}/>
                        <label htmlFor="user-edit-lastname">Nom</label>
                    </div>
                    <div className="field field-half">
                        <input id="user-edit-firstname" type="text" name="prenom" placeholder=" " defaultValue={this.state.user_edit.prenom} autoComplete="off" onChange={this.onChange}/>
                        <label htmlFor="user-edit-firstname">Prénom</label>
                    </div>
                    <div className="field field-half">
                        <input id="user-edit-birth" type="date" name="naissance" placeholder=" " defaultValue={this.state.user_edit.naissance} autoComplete="off" onChange={this.onChange}/>
                        <label htmlFor="user-edit-birth">Date de Naissance</label>
                    </div>
                    <div className="field field-half">
                        <input id="user-edit-email" type="email" name="email" placeholder=" " defaultValue={this.state.user_edit.email} autoComplete="off"  onChange={this.onChange}/>
                        <label htmlFor="user-edit-email">Adresse mail</label>
                    </div>
                    <div className="field">
                        <input id="user-edit-city" type="text" name="city" placeholder=" " ref='city' defaultValue={this.state.user_edit.nom_reel} autoComplete="off" onChange={this.onChange}/>
                        <label htmlFor="user-edit-city">Ville</label>
                        <div className="cities-suggest">
                            {
                                this.state.citiesSuggest.map(el => <span onClick={this.clickCity} id-city={el.id} key={el.id}> {el.nom_reel + ' (' + el.departement + ')' } </span>)
                            }
                        </div>
                    </div>
                    <div className="field field-half">
                        <input id="user-edit-promotion" type="text" name="promotion" placeholder=" " defaultValue={this.state.user_edit.promotion} autoComplete="off" onChange={this.onChange}/>
                        <label htmlFor="user-edit-promotion">Promotion</label>
                    </div>
                    <div className="field field-half">
                        <input id="user-edit-statut" type="text" name="statut" placeholder=" " defaultValue={this.state.user_edit.statut} autoComplete="off"  onChange={this.onChange}/>
                        <label htmlFor="user-edit-statut">Statut</label>
                    </div>

                      <div className="button grey margin" onClick={() => this.cancelForm()}>Annuler</div>
                      <div className="button " onClick={() => this.editUser()}>Valider</div>
                  </div>
                }
              </div>
            }
            </div>
          }
        </div>
    );
  }
}
