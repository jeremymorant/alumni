import React from 'react';
import ReactDOM from 'react-dom';

import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import History from "./services/History";
import {PostData} from "./services/PostData";

import Home from "./pages/home/Home";
import Family from "./pages/family/Family";
import Offres from "./pages/offres/Offers";
import Actualites from "./pages/actualites/Actualites";
import Agenda from "./pages/agenda/Agenda";
import Compte from "./pages/compte/Compte";
import Admin from "./pages/admin/Admin";
import Annuaire from "./pages/annuaire/Annuaire";

import Login from "./component/login/Login";
import LoginPage from "./component/login/LoginPage";
import Header from "./component/header/Header";
import Footer from "./component/footer/Footer";

import "./index.css";

import registerServiceWorker from './registerServiceWorker';

export default class App extends React.Component {


  constructor(){
    super();

    this.state = {
      ready: false,
      admin: false,
      isLoggedIn: false
    }

  }

  componentDidMount(){
      this.isLoggedIn();
  }

  isLoggedIn() {
      let sessionUser = sessionStorage.getItem('userData');

      if(sessionUser){
          sessionUser =  JSON.parse(sessionUser);
          PostData({
              api: 'login-validate',
              dataValidate: {
                  email: sessionUser.email,
                  token: sessionUser.token,
                  id: sessionUser.id
              }
          }).then((result) => {
              let responseJson = result;
              if (responseJson.error != null) {
                  this.setState({isLoggedIn: false, ready: true});
                  return false;
                  //History.replace('/');
              }else{
                  if(responseJson.success.admin == 1){
                    this.setState({isLoggedIn: true, ready: true, admin: true});
                  }else{
                    this.setState({isLoggedIn: true, ready: true});
                  }
                  return true;
              }
          });
      }else{
          this.setState({isLoggedIn: false, ready: true});
          return false;
          //History.replace('/');
      }
  }

  render () {
      return (
          <div className="App-Main">
              <Header />
              <Login />
                  { this.state.ready &&
                      <BrowserRouter>
                          <Switch>
                              <Redirect from="/home" to="/" />
                              <Route exact path="/" component={Home} />
                              <Route path="/actualites/:page?/:post_id?/:category_id?" component={Actualites} />

                              <Route path="/admin" render={(props) => (
                                this.state.isLoggedIn && this.state.admin ? (  <Admin {...props} /> ) : (  <LoginPage {...props}/>  )
                              )}/>
                              <Route path="/compte" render={(props) => (
                                this.state.isLoggedIn ? (  <Compte {...props} /> ) : (  <LoginPage {...props}/>  )
                              )}/>
                              <Route path="/famille" render={(props) => (
                                this.state.isLoggedIn ? ( <Family {...props} /> ) : (  <LoginPage {...props}/>  )
                              )}/>
                              <Route path="/offres"  render={(props) => (
                                this.state.isLoggedIn ? ( <Offres {...props} /> ) : (  <LoginPage {...props}/>  )
                              )}/>
                              <Route path="/agenda" render={(props) => (
                                this.state.isLoggedIn ? (  <Agenda {...props} /> ) : (  <LoginPage {...props}/>  )
                              )}/>
                              <Route path="/annuaire/:page?/:user_id?"  render={(props) => (
                                this.state.isLoggedIn ? (  <Annuaire {...props} /> ) : (<LoginPage {...props}/> )
                              )}/>
                          </Switch>
                      </BrowserRouter>
                  }
              <Footer/>
          </div>
        );
    }
}


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
