import React, {Component} from 'react';

import {PostData} from "../../services/PostData";

import UserInformations from "./row/UserInformations";
import UserCV from "./row/UserCV";
import UserSocial from "./row/UserSocial";
import HistoryOffers from "./row/HistoryOffers";
import HistoryEvents from "./row/HistoryEvents";
import "./Compte.css";

export default class Compte extends Component {

  constructor(){
      super();

      this.state = {
          user_details: [],
          offers: [],
          events: [],
          cv: '',
          notification: {
            color: '',
            title: '',
            description: ''
          }
      };

      this.getUserDatas(true);
  }

  callBackAvatar = (user_id) => {
    this.getUserDatas(false);
  }

  callBackCV = (user_id) => {
    this.getCVDatas(user_id);
  }

  callBackSocial = (user_id) => {
    this.getUserDatas(false);
  }

  getUserDatas = (getOthers) => {
    let sessionUser = sessionStorage.getItem('userData');

    if(sessionUser){
        sessionUser =  JSON.parse(sessionUser);

        PostData({
          api: 'account-user-informations',
          dataUser: {
            id: sessionUser.id,
            email: sessionUser.email,
            token: sessionUser.token
          }
        }).then((result) => {
            let responseJson = result;
            if (responseJson.userData) {
                this.setState({user_details: responseJson.userData});
                if(getOthers){
                  this.getCVDatas(sessionUser.id);
                  this.getOffersDatas(sessionUser.id);
                  this.getEventsDatas(sessionUser.id);
                }
            }else{
                this.setState({
                  user_details: [],
                  notification: {
                    color: 'red',
                    title: 'Erreur',
                    description: responseJson.error.text
                  }
                });
            }
        });
      }
  }

  getCVDatas = (sessionUser) => {
    PostData({api: 'account-user-cv', dataUser: {id: sessionUser}}).then((result) => {
        let responseJson = result;
        if (responseJson.cvData) {
            this.setState({cv: responseJson.cvData});
        }else{
            this.setState({
              cv: '',
              notification: {
                color: 'red',
                title: 'Erreur',
                description: responseJson.error.text
              }
            });
        }
    });
  }

  getOffersDatas = (sessionUser) => {
    PostData({api: 'account-history-offers', dataUser: {id: sessionUser}}).then((result) => {
        let responseJson = result;
        if (responseJson.offersData) {
            this.setState({offers: responseJson.offersData});
        }else{
            this.setState({
              offers: [],
              notification: {
                color: 'red',
                title: 'Erreur',
                description: responseJson.error.text
              }
            });
        }
    });
  }

  getEventsDatas = (sessionUser) => {
    PostData({api: 'account-history-events', dataUser: {id: sessionUser}}).then((result) => {
        let responseJson = result;
        if (responseJson.eventsData) {
            this.setState({events: responseJson.eventsData});
        }else{
            this.setState({
              events: [],
              notification: {
                color: 'red',
                title: 'Erreur',
                description: responseJson.error.text
              }
            });
        }
    });
  }


  render() {
    return (
      <div className="Account">
          <div className="Account-inner">

            <div className="Account-title">
              <h1>Mon Compte - Informations utiles</h1>
              <p>Vous trouverez ici les informations importantes permettant de prendre contact avec vous. Importez votre CV et remplissez les liens de vos réseaux sociaux. Vous pouvez aussi consulter les offres et évènements qui vous ont intéressé.</p>
            </div>

            <UserInformations
              userDatas={this.state.user_details}
              callBackAvatar={this.callBackAvatar}/>
            <UserCV
              callBackCV={this.callBackCV}
              cvDatas={this.state.cv}/>
            <UserSocial
              callBackSocial={this.callBackSocial}
              facebookDatas={this.state.user_details.facebook}
              twitterDatas={this.state.user_details.twitter}
              linkedDatas={this.state.user_details.linkedin}/>
            <HistoryOffers
              offersDatas={this.state.offers}/>
            <HistoryEvents
              eventsDatas={this.state.events}/>
          </div>
      </div>
    )
  }

}
