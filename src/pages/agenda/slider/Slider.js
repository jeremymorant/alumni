import React from 'react';
import Carousel from 'nuka-carousel';

import './Slider.css';


export default class extends React.Component {
  constructor(){
    super();

    this.state = {
      renderCenterLeftControls: null,
      renderCenterRightControls: null,
      wrapAround: 'true',
      autoplay: true,
      autoplayInterval: 5000
    }
  }

  render() {
    return (
        <div className="Slider">
          <Carousel
            renderCenterLeftControls={this.state.renderCenterLeftControls}
            renderCenterRightControls={this.state.renderCenterRightControls}
            autoplay={this.state.autoplay}
            autoplayInterval={this.state.autoplayInterval}
            heightMode={this.state.heightMode}
            wrapAround={this.state.wrapAround}
          >
            <img alt="Home1" src="https://via.placeholder.com/900x280/ffffff/c0392b/&text=Home1" />
            <img alt="Home2" src="https://via.placeholder.com/900x280/ffffff/c0392b/&text=Home2" />
            <img alt="Home3" src="https://via.placeholder.com/900x280/ffffff/c0392b/&text=Home3" />
            <img alt="Home4" src="https://via.placeholder.com/900x280/ffffff/c0392b/&text=Home4" />
          </Carousel>
        </div>
    );
  }
}
