import React, {Component} from 'react';

import {PostData} from '../../services/PostData';
import './Login.css';

import FaUser from 'react-icons/lib/fa/user';
import FaLogout from 'react-icons/lib/fa/sign-out';

import PopupNotification from './popup/PopupNotification';

export default class Login extends Component {
    constructor(){
        super();
        this.state = { api: 'login', email: '', password: '', redirectToReferrer: false, loginActive: '', notification: {color: '', title: '', description: ''} };
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.loginShow = this.loginShow.bind(this);
        this.loginHide = this.loginHide.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    loginShow(){
        this.setState({loginActive: "active"});
    }

    loginHide(){
        this.setState({loginActive: ""});
    }

    login() {
        if(this.state.email && this.state.password){
            PostData(this.state).then((result) => {
                let responseJson = result;
                if(responseJson.userData){
                    sessionStorage.setItem('userData',JSON.stringify(responseJson.userData));
                    this.setState({redirectToReferrer: true});
                }else{
                  this.setState({ notification : {
                    color: 'red',
                    title: 'Erreur',
                    description: responseJson.error.text
                  }});
                  this.notificationChild.showNotification();
                }
            });
        }
    }

    logout() {
        sessionStorage.clear();
        window.location = "/home";
    }

    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }

    render() {
        if (sessionStorage.getItem('userData')){
            let userData = JSON.parse(sessionStorage.getItem('userData'));
            return (
                <div id="login-container-logged">
                    <div className="login-inner-logged">
                        <div className="names">
                            <a key="link-to-account" href="compte"><p>{userData.prenom} {userData.nom}</p></a>
                        </div>
                        <div className="actions">
                            <FaLogout onClick={this.logout} />
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div>
                <div className={this.state.loginActive + " login-show"} id="login-show" onClick={this.loginShow}>
                    <span>Accès membre</span>
                    <FaUser/>
                </div>
                <div className={this.state.loginActive + " login-container" } id="login-container">
                    <div className="login-top" onClick={this.loginHide}>
                        <span> &larr; </span>
                    </div>

                    <div className="login-inner">
                        <p className="login-title">Accès membre</p>
                        <div className="field">
                            <input id="login-email" type="text" name="email" placeholder=" " onChange={this.onChange}/>
                            <label htmlFor="login-email">Adresse email</label>
                        </div>
                        <div className="field">
                            <input id="login-password" type="password" name="password" placeholder=" " onChange={this.onChange}/>
                            <label htmlFor="login-password">Mot de passe</label>
                        </div>

                        <input type="submit" value="Valider" onClick={this.login}/>

                        <div className="links">
                            <p>Mot de passe oublié ?</p>
                            <p>Pas encore de compte ?</p>
                        </div>
                    </div>

                    <div className="login-footer">

                    </div>
                </div>
                <PopupNotification
                  ref={instance => { this.notificationChild = instance; }}
                  color={this.state.notification.color}
                  title={this.state.notification.title}
                  description={this.state.notification.description} />
              </div>
        );
    }


}
