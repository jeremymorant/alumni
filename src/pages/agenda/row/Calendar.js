import React from 'react';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer. // or globalizeLocalizer

import 'react-big-calendar/lib/css/react-big-calendar.css'
import './Calendar.css';

export default class Calendar extends React.Component {

  componentDidMount(){
  }

  handleClickToday = () => {
    this.props.dateChangedCallBack("today");

    let toolsLeft = document.getElementsByClassName("rbc-btn-group")[1];
    let toolPrevious = toolsLeft.getElementsByTagName("button")[1];

    toolPrevious.click();
  }

  handleClickPrevious = () => {
    this.props.dateChangedCallBack("moins");

    let toolsLeft = document.getElementsByClassName("rbc-btn-group")[1];
    let toolPrevious = toolsLeft.getElementsByTagName("button")[1];

    toolPrevious.click();
  }

  handleClickNext = () => {
    this.props.dateChangedCallBack("plus");

    let toolsLeft = document.getElementsByClassName("rbc-btn-group")[1];
    let toolNext = toolsLeft.getElementsByTagName("button")[2];

    toolNext.click();
  }

  handleClickEvent = (e, a) => {
      this.itemClicked(e.id);
  }

  itemClicked(id){

    this.props.dateClickCallBack(id);
  }

  render() {

    BigCalendar.momentLocalizer(moment);

    return (
            <div className="bigCalendar" >


                <div className="rbc-toolbar">
                  <span className="rbc-btn-group">
                    <button type="button" onClick={this.handleClickToday}>ajourd&nbsp;hui</button>
                    <button type="button"  onClick={this.handleClickPrevious}>&lt;</button>
                    <button type="button"  onClick={this.handleClickNext}>&gt;</button>
                  </span>
                </div>

                <BigCalendar className="demo"
                  events={this.props.events}
                  startAccessor='startDate'
                  endAccessor='endDate'
                  step={60}
                  showMultiDayTimes
                  defaultView='week'
                  onSelectEvent={this.handleClickEvent}
                />
          </div>

    );
  }
}
