import React, {Component} from 'react';


import UserList from "./row/UserList";
import UserSearch from "./row/UserSearch";
import UserDetails from "./row/UserDetails";

export default class Annuaire extends Component {

  render() {
    const queryString = require('query-string');
    var parsed = queryString.parse(this.props.location.search);

    if(parsed.user_id && !parsed.page){
    return (
      <div className="Annuaire">
        <UserDetails post_id={parsed.user_id}/>
      </div>
    );}
    else if(parsed.page && !parsed.user_id){
      return (
        <div className="Annuaire">
          <UserList page={parsed.page}/>
        </div>
    );}
    else {
      return (
        <div className="Annuaire">
          <UserList page={1}/>
        </div>
    );}
  }

}
