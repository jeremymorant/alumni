import React from 'react';
import axios, {post} from 'axios';

import './UserInformations.css';
import {PostData} from "../../../services/PostData";
import FaWarning from 'react-icons/lib/fa/exclamation-triangle';

export default class UserInformations extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      avatarFile: null,
      citiesSuggest: []
    }

    this.importAvatar = this.importAvatar.bind(this);
    this.avatarChanged = this.avatarChanged.bind(this);
    this.editInformations = this.editInformations.bind(this);

  }


  renderImage = (imageUrl) => {
      return ( <img src={`${imageUrl}?${Date.now()}`}  alt="user avatar"/> );
  }

  importAvatar = () => {
      this.refs.avatar_hidden.click();
  }

  editInformations = () => {
    let sessionUser = sessionStorage.getItem('userData');
        sessionUser =  JSON.parse(sessionUser);

    this.setState(
      {user_id: sessionUser.id},
      () => {
        console.log(this.state);
        PostData({
          api: 'account-user-informations-edit',
          dataUser: this.state
        }).then((result) => {
          this.props.callBackAvatar(sessionUser.id);
        });
      }
    );
  }

  clickCity = (e) => {
      this.refs.city.value = e.target.innerHTML;
      this.setState({
        city_id: e.target.getAttribute("id-city"),
        citiesSuggest: []
      });
  }


  onChange = (e) => {
    if(e.target.name === "city"){
        if(e.target.value.length < 3){
            this.setState({citiesSuggest: []});
        }else {
            PostData({api: 'cities-search', text: e.target.value}).then((result) => {
                let responseJson = result;
                if(responseJson.citiesData == null){
                    this.setState({citiesSuggest: []});
                }else if (responseJson.citiesData.length > 0) {
                    this.setState({citiesSuggest: responseJson.citiesData});
                }else{
                    this.setState({citiesSuggest: []});
                }
            });
        }
        switch(e.target.type){
            case 'checkbox':
                this.setState({[e.target.value]:e.target.checked});
                break;
            default:
                this.setState({[e.target.name]:e.target.value});
        }
    }else {
        switch(e.target.type){
            case 'checkbox':
                this.setState({[e.target.value]:e.target.checked});
                break;
            default:
                this.setState({[e.target.name]:e.target.value});
        }
    }
  }

  avatarChanged = (e) => {
    let fData = new FormData();
    let avatarFile = e.target.files[0];
    let sessionUser = sessionStorage.getItem('userData');
        sessionUser =  JSON.parse(sessionUser);

    fData.append('avatar', avatarFile, { type: 'image' });
    fData.append('api',"account-avatar-edit");
    fData.append('user_id', sessionUser.id);

    axios.post('https://jeremy-morant.000webhostapp.com/index.php', fData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    } )
    .then( (response) => {
      this.props.callBackAvatar(false);
    }).catch((error) => {
      console.log(error);
    });
  }


  render() {
    return (
        <div className="user-informations">
            <div className="user-informations-right">
              <div className="avatar-container">
                <img src={`${this.props.userDatas.avatar}?${Date.now()}`}  alt="user avatar"/>
              </div>
              <div className="button" onClick={this.importAvatar}>Importer un avatar</div>
              <input type="file" className="avatar_hidden" accept="image/*" ref="avatar_hidden" onChange={this.avatarChanged}/>
            </div>

            <div className="user-informations-left">
                <div className="field field-half readOnly">
                    <input id="user-informations-lastname" type="text" name="nom" placeholder=" " defaultValue={this.props.userDatas.nom} autoComplete="off" readOnly/>
                    <label htmlFor="user-informations-name">Nom</label>
                </div>
                <div className="field field-half readOnly">
                    <input id="user-informations-firstname" type="text" name="prenom" placeholder=" " defaultValue={this.props.userDatas.prenom} autoComplete="off" readOnly/>
                    <label htmlFor="user-informations-name">Prénom</label>
                </div>
                <div className="field field-half readOnly">
                    <input id="user-informations-birth" type="date" name="naissance" placeholder=" " defaultValue={this.props.userDatas.naissance} autoComplete="off" readOnly/>
                    <label htmlFor="user-informations-birth">Date de Naissance</label>
                </div>
                <div className="field field-half readOnly">
                    <input id="user-informations-promotion" type="text" name="promotion" placeholder=" " defaultValue={this.props.userDatas.promotion} autoComplete="off" readOnly/>
                    <label htmlFor="user-informations-promotion">Promotion</label>
                </div>
                <div className="field">
                    <input id="user-informations-city" type="text" name="city" placeholder=" " ref='city' defaultValue={this.props.userDatas.nom_reel} autoComplete="off"  onChange={this.onChange}/>
                    <label htmlFor="user-informations-city">Ville</label>
                    <div className="cities-suggest">
                        {
                            this.state.citiesSuggest.map(el => <span onClick={this.clickCity} id-city={el.id} key={el.id}> {el.nom_reel + ' (' + el.departement + ')' } </span>)
                        }
                    </div>
                </div>
                <div className="field">
                    <input id="user-informations-email" type="email" name="email" placeholder=" " defaultValue={this.props.userDatas.email} autoComplete="off"  onChange={this.onChange}/>
                    <label htmlFor="user-informations-email">Adresse mail <FaWarning /> (identifiant de connexion)</label>
                </div>
                <div className="field field-half">
                    <input id="user-informations-phone" type="phone" name="phone" placeholder=" " defaultValue={this.props.userDatas.phone} autoComplete="off"  onChange={this.onChange}/>
                    <label htmlFor="user-informations-phone">Téléphone</label>
                </div>
                <div className="field field-half">
                  <div className="button" onClick={this.editInformations}>Valider</div>
                </div>
            </div>
        </div>
    );
  }
}
