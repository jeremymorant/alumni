import React from 'react';
import axios, {post} from 'axios';
import {PostData} from '../../../services/PostData';

import './UserCV.css';

export default class UserCV extends React.Component {

  importCV = () => {
      this.refs.cv_hidden.click();
  }

  deleteCV = () => {
    let sessionUser = sessionStorage.getItem('userData');
        sessionUser =  JSON.parse(sessionUser);

    PostData({api: 'account-cv-remove', user_id: sessionUser.id}).then((result) => {
      this.props.callBackAvatar(sessionUser.id);
    });
  }

  cvChanged = (e) => {
    let fData = new FormData();
    let cvFile = e.target.files[0];
    let sessionUser = sessionStorage.getItem('userData');
        sessionUser =  JSON.parse(sessionUser);

    fData.append('cv', cvFile);
    fData.append('api',"account-pdf-edit");
    fData.append('user_id', sessionUser.id);

    axios.post('https://jeremy-morant.000webhostapp.com/index.php', fData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    } )
    .then( (response) => {
      this.props.callBackAvatar(sessionUser.id);
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    return (
      <div className="user-cv">
            <div className="user-cv-inner">
              <div className="user-cv-title">
                Gestion de mon CV
              </div>
              <div className="button" onClick={()=> window.open(this.props.cvDatas.cv_path, "_blank")}>Consulter</div>
              <div className="button" onClick={this.importCV}>Importer</div>
              <div className="button" onClick={this.deleteCV}>Supprimer</div>
              <input type="file" className="cv-hidden" accept="application/pdf" ref="cv_hidden" onChange={this.cvChanged}/>
            </div>
        </div>
    );
  }
}
