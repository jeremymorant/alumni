import React from 'react';
import Carousel from 'nuka-carousel';

import './Slider.css';


export default class extends React.Component {
  constructor(){
    super();

    this.state = {
      renderCenterLeftControls: null,
      renderCenterRightControls: null,
      wrapAround: true,
      autoplay: true,
      autoplayInterval: 5000,
      heightMode: 'first'
    }
  }

  render() {
    return (
        <div className="Slider">
          <Carousel
            renderCenterLeftControls={this.state.renderCenterLeftControls}
            renderCenterRightControls={this.state.renderCenterRightControls}
            autoplay={this.state.autoplay}
            autoplayInterval={this.state.autoplayInterval}
            heightMode={this.state.heightMode}
            wrapAround={this.state.wrapAroun}
          >
            <img alt="Home1" src={require('./images/reseau.jpg')} />
            <img alt="Home2" src={require('./images/contrat.jpg')} />
            <img alt="Home3" src={require('./images/afterwork.jpg')}/>
          </Carousel>
        </div>
    );
  }
}
