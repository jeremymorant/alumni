import React from 'react';

import './RowSummary.css';
import {PostData} from "../../../services/PostData";

export default class extends React.Component {
  constructor(){
    super();
    this.state = { api: 'home-offers', offers: [], events: [], redirectToReferrer: false};
    this.getOffers = this.getOffers.bind(this);
    this.getEvents = this.getEvents.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  getOffers() {
    PostData(this.state).then((result) => {
        let responseJson = result;
        if(responseJson.offers){
            this.setState({
              api: 'home-events',
              offers: responseJson.offers}, () =>
              {
                this.getEvents();
              }
            );
        }
    });
  }

  getEvents() {
    PostData(this.state).then((result) => {
        let responseJson = result;
        if(responseJson.events){
            this.setState({
              events: responseJson.events,
              redirectToReferrer: true});
        }
    });
  }

  componentDidMount(){
      this.getOffers();
      //this.setState({redirectToReferrer: false})
      //this.getEvents();
  }
    render() {
        return (
            <div className="RowSummary-container">

                <div className="RowSummary-item">
                    <div className="RowSummary-item-title">
                        Offres d&apos;emploi
                    </div>
                    <div className="RowSummary-item-body">
                      {
                        this.state.offers.map(el =>
                          <div className="RowSummary-item-offer" key={'offer-id-'+el.id}>
                              <span className="RowSummary-item-offer-type" key={'offer-type-id-'+el.id}>{el.type}</span>
                              <span className="RowSummary-item-offer-title" key={'offer-title-id-'+el.id}>{el.title}</span>
                              <span className="RowSummary-item-offer-subtitle" key={'offer-company-id-'+el.id}>{el.company}</span>
                          </div>
                        )
                      }
                    </div>
                    <div className="RowSummary-more">
                        <a href="/offres"><span className="RowSummary-more-button">En savoir +</span></a>
                    </div>
                </div>

                <div className="RowSummary-item">
                    <div className="RowSummary-item-title">
                        Agenda
                    </div>
                    <div className="RowSummary-item-body">
                      {
                        this.state.events.map(el =>
                          <div className="RowSummary-item-offer" key={'event-id-'+el.id}>
                              <span className="RowSummary-item-offer-type" key={'event-category-id-'+el.id}>{el.category}</span>
                              <span className="RowSummary-item-offer-title" key={'event-title-id-'+el.id}>{el.title}</span>
                              <span className="RowSummary-item-offer-subtitle" key={'event-city-id-'+el.id}>{el.city}</span>
                          </div>
                        )
                      }
                    </div>
                    <div className="RowSummary-more">
                        <a href="/agenda"><span className="RowSummary-more-button">En savoir +</span></a>
                    </div>
                </div>

            </div>
        );
    }
}
