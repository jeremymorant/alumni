import React from 'react';

import {PostData} from "../../../../services/PostData";

import './Events.css';
import FaUser from 'react-icons/lib/fa/calendar';
import FaCross from 'react-icons/lib/fa/times-circle';
import FaEdit from 'react-icons/lib/fa/edit';
import FaAdd from 'react-icons/lib/fa/plus';
import FaStar from 'react-icons/lib/fa/star';
import FaRemove from 'react-icons/lib/fa/trash';

export default class Events extends React.Component {

  constructor(){
    super();

    this.state = {
      citiesSuggest: [],
      editor: false,
      star: false,
      edit: false,
      remove: false,
      title: '',
      description: '',
      city_id: '',
      address: '',
      category_id: '',
      start_date: '',
      end_date: '',
      hasStarFirst: false,
      hasStarSecond: false,
      starFirst: [],
      starSecond: [],
      events: [],
      event_edit: ''
    }

  }

  componentWillMount(){
      this.getStarEvents();
      this.getEvents();
  }

  getStarEvents = () => {
    PostData({api: 'agenda-star'}).then((result) => {
        let responseJson = result;

        if(responseJson.agendaData){

          responseJson = responseJson.agendaData;
          if(responseJson.length > 0){
              this.setState({
                hasStarFirst: true,
                starFirst: responseJson[0]
              });
          }else{
              this.setState({
                hasStarFirst: false,
                starFirst: []
              });
          }
          if(responseJson.length > 1){
              this.setState({
                hasStarSecond: true,
                starSecond: responseJson[1]
              });
          }else{
              this.setState({
                hasStarSecond: false,
                starSecond: []
              });
          }
        }else{
            this.setState({
              hasStarFirst: false,
              starFirst: [],
              hasStarSecond: false,
              starSecond: []
            });
        }
    });
  }

  getEvents = () => {

    PostData({api: 'agenda-nexts'}).then((result) => {
        let responseJson = result;

        if(responseJson.agendaData){
            this.setState({
              events: responseJson.agendaData
            });
        }else{
            this.setState({
              events: []
            });
        }
    });

  }

  clickAction = action => {
    switch(action){
      case 'star':
        this.setState({editor: true, star: true, add: false, edit: false, remove: false});
        break;
      case 'add':
        this.setState({editor: true, star: false, add: true, edit: false, remove: false,
        title: '',
        description: '',
        city_id: '',
        address: '',
        category_id: '',
        start_date: '',
        end_date: '',
        time_start: '',
        time_end: ''});
        break;
      case 'edit':
        this.setState({editor: true, star: false, add: false, edit: true, remove: false,
          title: '',
          description: '',
          city_id: '',
          address: '',
          category_id: '',
          start_date: '',
          end_date: '',
          time_start: '',
          time_end: ''
        });
        break;
      case 'remove':
        this.setState({editor: true, star: false, add: false, edit: false, remove: true});
        break;
    }
  }

  clickCity = (e) => {
      this.refs.city.value = e.target.innerHTML;
      this.setState({
        city_id: e.target.getAttribute("id-city"),
        citiesSuggest: []
      });
  }

  clickEventToEdit = (id) => {
    PostData({api: 'agenda-unique', dataAgenda : {id: id}}).then((result) => {
        let responseJson = result;
        if(responseJson.agendaData){
          this.setState({
            event_edit: responseJson.agendaData
          });
        }
      });
  }

  clickEventToStar = (id) => {
    if(!this.state.hasStarFirst){
      PostData({api: 'agenda-unique', dataAgenda : {id: id}}).then((result) => {
          let responseJson = result;
          if(responseJson.agendaData){
            this.setState({
              hasStarFirst: true,
              starFirst: responseJson.agendaData
            });
          }
        });
    }else if(!this.state.hasStarSecond){
      PostData({api: 'agenda-unique', dataAgenda : {id: id}}).then((result) => {
          let responseJson = result;
          if(responseJson.agendaData){
            this.setState({
              hasStarSecond: true,
              starSecond: responseJson.agendaData
            });
          }
      });
    }
  }

  clickEventToDelete = (id) => {
    if(window.confirm('Voulez-vous vraiment le supprimer ?')){
      PostData({api: 'agenda-remove', dataAgenda: {id: id}}).then((result) => {
        this.setState({
          editor: false,
          remove: false,
          event_edit: ''
        });

        this.getEvents();
      });
    }
  }

  onChange = (e) => {
    if(e.target.name === "city"){
        if(e.target.value.length < 3){
            this.setState({citiesSuggest: []});
        }else {
          PostData({api: 'cities-search', text: e.target.value}).then((result) => {
              let responseJson = result;
              if(responseJson.citiesData == null){
                  this.setState({citiesSuggest: []});
              }else if (responseJson.citiesData.length > 0) {
                  this.setState({citiesSuggest: responseJson.citiesData});
              }else{
                  this.setState({citiesSuggest: []});
              }
          });
        }
        switch(e.target.type){
            case 'checkbox':
                this.setState({[e.target.value]:e.target.checked});
                break;
            default:
                this.setState({[e.target.name]:e.target.value});
        }
    }else {
        switch(e.target.type){
            case 'checkbox':
                this.setState({[e.target.value]:e.target.checked});
                break;
            default:
                this.setState({[e.target.name]:e.target.value});
        }
    }
  }

  removeStar = (num) => {
    if(num == 1) {
      this.setState({
          hasStarFirst: false,
          starFirst: [],
      });
    }else{
      this.setState({
        hasStarSecond: false,
        starSecond: []
      })
    }
  }

  addEvent = () => {
    PostData({api: 'agenda-add', dataAgenda : this.state}).then((result) => {
        let responseJson = result;

          this.setState({
            editor: false,
            add: false,
            event_edit: ''
          });

          this.getEvents();
    });
  }

  editStar = () => {
    PostData({api: 'agenda-edit-star', dataAgenda : this.state}).then((result) => {
        let responseJson = result;

          this.setState({
            editor: false,
            star: false,
            event_edit: ''
          });

          this.getStarEvents();
    });
  }

  editEvent = () => {
    PostData({api: 'agenda-edit', dataAgenda : this.state}).then((result) => {
        let responseJson = result;

          this.setState({
            editor: false,
            edit: false,
            event_edit: ''
          });

          this.getEvents();
    });

  }

  cancelForm = () => {
    if(this.state.star){
      this.getStarEvents();
    }
    this.setState({editor: false, star: false, add: false, edit: false, event_edit: '', remove: false});
  }



  render() {
    return (
        <div className="admin-row admin-row-large">
          <div className="title-icon">
            <FaUser />
          </div>
          <div className="title">
            Agenda des évènements
          </div>
          <div className="actions">
            <div className="action-item star" onClick={() => this.clickAction("star")}><FaStar /></div>
            <div className="action-item add" onClick={() => this.clickAction("add")}><FaAdd /></div>
            <div className="action-item edit" onClick={() => this.clickAction("edit")}><FaEdit /></div>
            <div className="action-item remove" onClick={() => this.clickAction("remove")}><FaRemove /></div>
          </div>

          { this.state.editor &&
            <div className="editor-container events-editor-container">
              { this.state.star &&
                <div className="events-editor-star">
                  <div className="events-editor-add-block">
                      <div className="events-editor-star-title">
                        Choisissez les deux évènements importants à mettre en avant sur la page d&apos;agenda.
                      </div>
                      { this.state.hasStarFirst &&
                        <div className="event-star">
                            <span onClick={() => this.removeStar(1)} className="event-star-remove">
                              <FaCross />
                            </span>
                            <div className="event-star-title">
                               {this.state.starFirst.title}
                            </div>
                            <div className="event-star-date">
                               {this.state.starFirst.startDate}
                            </div>
                        </div>
                      }
                      { this.state.hasStarSecond &&
                        <div className="event-star">
                            <span onClick={() => this.removeStar(2)} className="event-star-remove">
                              <FaCross />
                            </span>
                            <div className="event-star-title">
                               {this.state.starSecond.title}
                            </div>
                            <div className="event-star-date">
                               {this.state.starSecond.startDate}
                            </div>
                        </div>
                      }
                      <div className="button margin" onClick={() => this.editStar()}>Valider</div>
                      <div className="button grey" onClick={() => this.cancelForm()}>Annuler</div>
                  </div>
                  <div className="events-editor-add-block events-list">
                      {
                          this.state.events.map(el => <div onClick={() => this.clickEventToStar(el.id)} key={'events-star-' + el.id}><div>{el.title}</div><div>{el.start_date}</div> </div> )
                      }
                  </div>
                </div>
              }
              { this.state.add &&
                <div className="events-editor-add">
                  <div className="events-editor-add-block">

                      <div className="field">
                          <input id="admin-events-add-title" type="text" name="title" placeholder=" "  autoComplete="off" onChange={this.onChange}/>
                          <label htmlFor="admin-events-add-title">Titre</label>
                      </div>

                      <div className="field">
                          <input id="admin-events-add-city" type="text" name="city" placeholder=" " ref='city' autoComplete="off" onChange={this.onChange}/>
                          <label htmlFor="admin-events-add-city">Ville</label>
                          <div className="cities-suggest">
                              {
                                  this.state.citiesSuggest.map(el => <span onClick={this.clickCity} id-city={el.id} key={el.id}> {el.nom_reel + ' (' + el.departement + ')' } </span>)
                              }
                          </div>
                      </div>

                      <div className="field field-half">
                          <input id="admin-events-add-address" type="text" name="address" placeholder=" "  autoComplete="off" onChange={this.onChange} />
                          <label htmlFor="admin-events-add-address">Adresse</label>
                      </div>

                      <div className="field field-half">
                          <select id="admin-events-add-category" name="category_id" placeholder=" " onChange={this.onChange}>
                            <option value=""></option>
                            <option value="1">Afterwork</option>
                            <option value="2">Gala</option>
                            <option value="3">MIAGE</option>
                            <option value="4">Informatique</option>
                            <option value="5">Gestion</option>
                            <option value="6">Familles</option>
                          </select>
                          <label htmlFor="admin-events-add-category">Categorie</label>
                      </div>

                      <div className="field field-half">
                          <input id="admin-events-add-date-start" type="date" name="date_start" placeholder=" "  autoComplete="off" onChange={this.onChange} />
                          <label htmlFor="admin-events-add-date-start">Date de début</label>
                      </div>

                      <div className="field field-half">
                          <input id="admin-events-add-time-start" type="time" name="time_start" placeholder=" "  autoComplete="off" onChange={this.onChange}/>
                          <label htmlFor="admin-events-add-time-start">Heure</label>
                      </div>

                      <div className="field field-half">
                          <input id="admin-events-add-date-end" type="date" name="date_end" placeholder=" "  autoComplete="off" onChange={this.onChange}/>
                          <label htmlFor="admin-events-add-date-end">Date de fin</label>
                      </div>

                      <div className="field field-half">
                          <input id="admin-events-add-time-end" type="time" name="time_end" placeholder=" "  autoComplete="off" onChange={this.onChange}/>
                          <label htmlFor="admin-events-add-time-end">Heure</label>
                      </div>

                  </div>
                  <div className="events-editor-add-block">
                      <label htmlFor="admin-events-add-description">Décrivez l&apos;évènement</label>
                      <textarea id="admin-events-add-description" name="message" onChange={this.onChange}/>

                      <div className="button margin" onClick={() => this.addEvent()}>Ajouter l&apos;évènement</div>
                      <div className="button grey" onClick={() => this.cancelForm()}>Annuler</div>
                  </div>
                </div>
              }
              { this.state.edit &&
                <div className="events-editor-edit">
                  { !this.state.event_edit &&
                    <div className="events-editor-add-block events-list events-list-edit">
                        <span className="events-editor-star-title">
                          Choisissez premièrement l&apos;évènement que vous souhaitez modifier.
                        </span>
                        <span className="button grey" onClick={() => this.cancelForm()}>Annuler</span>
                        {
                            this.state.events.map(el => <div onClick={() => this.clickEventToEdit(el.id)} key={'events-star-' + el.id}><div>{el.title}</div><div>{el.start_date}</div> </div> )
                        }
                    </div>
                  }
                  { this.state.event_edit &&
                    <div className="event-editor-edit-form">
                      <div className="events-editor-add-block">

                          <div className="field">
                              <input id="admin-events-add-title" type="text" name="title" placeholder=" "  autoComplete="off" defaultValue={this.state.event_edit.title} onChange={this.onChange}/>
                              <label htmlFor="admin-events-add-title">Titre</label>
                          </div>

                          <div className="field">
                              <input id="admin-events-add-city" type="text" name="city" placeholder=" " ref='city' autoComplete="off" defaultValue={this.state.event_edit.nom_reel} onChange={this.onChange}/>
                              <label htmlFor="admin-events-add-city">Ville</label>
                              <div className="cities-suggest">
                                  {
                                      this.state.citiesSuggest.map(el => <span onClick={this.clickCity} id-city={el.id} key={el.id}> {el.nom_reel + ' (' + el.departement + ')' } </span>)
                                  }
                              </div>
                          </div>

                          <div className="field field-half">
                              <input id="admin-events-add-address" type="text" name="address" placeholder=" "  autoComplete="off" defaultValue={this.state.event_edit.address} onChange={this.onChange} />
                              <label htmlFor="admin-events-add-address">Adresse</label>
                          </div>

                          <div className="field field-half">
                              <select id="admin-events-add-category" name="category_id" placeholder=" " defaultValue={this.state.event_edit.category_id} onChange={this.onChange} >
                                <option value="" ></option>
                                <option value="1" selected={ (this.state.event_edit.category_id == 1) ? "selected" : "false" }>Afterwork</option>
                                <option value="2" selected={ (this.state.event_edit.category_id == 2) ? "selected" : "false" }>Gala</option>
                                <option value="3" selected={ (this.state.event_edit.category_id == 3) ? "selected" : "false" }>MIAGE</option>
                                <option value="4" selected={ (this.state.event_edit.category_id == 4) ? "selected" : "false" }>Informatique</option>
                                <option value="5" selected={ (this.state.event_edit.category_id == 5) ? "selected" : "false" }>Gestion</option>
                                <option value="6" selected={ (this.state.event_edit.category_id == 6) ? "selected" : "false" }>Familles</option>
                              </select>
                              <label htmlFor="admin-events-add-category">Categorie</label>
                          </div>

                          <div className="field field-half">
                              <input id="admin-events-add-date-start" type="date" name="date_start" placeholder=" "  autoComplete="off" defaultValue={this.state.event_edit.startDate.split(" ")[0]} onChange={this.onChange} />
                              <label htmlFor="admin-events-add-date-start">Date de début</label>
                          </div>

                          <div className="field field-half">
                              <input id="admin-events-add-time-start" type="time" name="time_start" placeholder=" "  autoComplete="off" defaultValue={this.state.event_edit.startDate.split(" ")[1]} onChange={this.onChange}/>
                              <label htmlFor="admin-events-add-time-start">Heure</label>
                          </div>

                          <div className="field field-half">
                              <input id="admin-events-add-date-end" type="date" name="date_end" placeholder=" "  autoComplete="off" defaultValue={this.state.event_edit.endDate.split(" ")[0]} onChange={this.onChange}/>
                              <label htmlFor="admin-events-add-date-end">Date de fin</label>
                          </div>

                          <div className="field field-half">
                              <input id="admin-events-add-time-end" type="time" name="time_end" placeholder=" "  autoComplete="off" defaultValue={this.state.event_edit.endDate.split(" ")[1]} onChange={this.onChange}/>
                              <label htmlFor="admin-events-add-time-end">Heure</label>
                          </div>

                      </div>
                      <div className="events-editor-add-block">
                          <label htmlFor="admin-events-add-description">Décrivez l&apos;évènement</label>
                          <textarea id="admin-events-add-description" name="message" defaultValue={this.state.event_edit.description} onChange={this.onChange} />

                          <div className="button margin" onClick={() => this.editEvent()}>Modifier l&apos;évènement</div>
                          <div className="button grey" onClick={() => this.cancelForm()}>Annuler</div>
                      </div>
                    </div>
                  }
                </div>
              }
              { this.state.remove &&
                <div className="events-editor-remove">
                  <div className="events-editor-add-block events-list events-list-edit">
                      <span className="events-editor-star-title">
                        Choisissez l&apos;évènement que vous voulez supprimer.
                      </span>
                      <span className="button grey" onClick={() => this.cancelForm()}>Annuler</span>
                      {
                          this.state.events.map(el => <div onClick={() => this.clickEventToDelete(el.id)} key={'events-star-' + el.id}><div>{el.title}</div><div>{el.start_date}</div> </div> )
                      }
                  </div>
                </div>
              }
            </div>
          }
        </div>
    );
  }
}
