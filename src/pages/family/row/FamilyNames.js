import React from 'react';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import * as FontAwesome from 'react-icons/lib/fa'

import './FamilyNames.css'

const SortableItem = SortableElement(({id,nom}) =>
    <li className="familyMember">{id} : {nom} <FontAwesome.FaBeer/> </li>
);

const SortableList = SortableContainer(({users}) => {
    return (
        <ul>
            {users.map((value, index) => (
                <SortableItem key={`item-${value.id}`} index={index} nom={value.name} id={value.id} />
            ))}
        </ul>
    );
});



export default class FamilyNames extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            users: []
        };
    } componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        users: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        this.setState({
            users: arrayMove(this.state.users, oldIndex, newIndex),
        });
    };


    render() {
        const { error, isLoaded } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="familyBody">
                    <h2>Nos Familles</h2>
                    <div className="sortableList">
                        <h2 className="familyTitle"> Famille 1 </h2>
                        <hr></hr>
                        <SortableList users={this.state.users} onSortEnd={this.onSortEnd}/>
                    </div>
                    <div className="sortableList">
                        <h2 className="familyTitle"> Famille 2 </h2>
                        <hr></hr>
                        <SortableList users={this.state.users} onSortEnd={this.onSortEnd}/>
                    </div>
                    <div className="sortableList">
                        <h2 className="familyTitle"> Famille 3 </h2>
                        <hr></hr>
                        <SortableList users={this.state.users} onSortEnd={this.onSortEnd}/>
                    </div>
                </div>
            );
        }
    }
}