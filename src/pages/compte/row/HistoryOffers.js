import React from 'react';

import FaCross from 'react-icons/lib/fa/times-circle';
import { PostData } from '../../../services/PostData';
import './HistoryOffers.css';

export default class HistoryOffers extends React.Component {

  constructor(){
    super();

    this.state = {
      popupOfferDetails: false
    }
  }

  offerDetails = (id) => {
      PostData({api: 'offer-details', dataSearch: {id: id}}).then((result) => {
          let responseJson = result;

          if (responseJson.offersData) {

              let datas = responseJson.offersData;
              this.setState({offer_details: {
                  offer_id: datas.id,
                  offer_title: datas.title,
                  offer_type: datas.type,
                  offer_company: datas.company_name,
                  offer_description: datas.description,
                  offer_city: datas.nom_reel,
                  offer_cp: datas.code_postal
              }});

              this.setState({offerDetails: true, offerClicked: id});
              this.showPopup();
          }
      });
  };


  hidePopup = () => {
      this.setState({popupOfferDetails: false});
  }

  showPopup = () => {
      this.setState({popupOfferDetails: true});
  }

  render() {
    return (
        <div className="history-offers">
            <div className="history-offers-inner">
                <div className="history-offers-title">
                    Dernières offres postulées
                </div>
                <div className="offers-item-list">
                {
                    this.props.offersDatas.map(el =>
                        <div className="offer-item" onClick={() => this.offerDetails(el.id)} key={'offer-'+el.id}>
                            <div className="offer-item-left">
                                <div className="offer-type" key={'offer-type-'+el.id}>{el.type}</div>
                            </div>
                            <div className="offer-item-right">
                                <div className="offer-title" key={'offer-title-'+el.id}>{el.title}</div>
                                <div className="offer-company" key={'offer-company-'+el.id}>Offre de : {el.company_name}</div>
                                <div className="offer-description" key={'offer-description-'+el.id}>{el.description}</div>
                            </div>
                        </div>
                    )
                }
                </div>
            </div>

            { this.state.popupOfferDetails &&
              <div className='popup'>
                  <div className="popup-overlay" onClick={this.hidePopup}> </div>
                  <div className="popup-container">
                      <div className="popup-details">
                          <div className="popup-header">
                              <span className="popup-header-title">Détails de l&apos;offre</span>
                              <FaCross className="popup-cross" onClick={this.hidePopup}/>
                          </div>
                          <div className="popup-body">
                              <div className="popup-body-title">
                                  {this.state.offer_details.offer_title}
                              </div>
                              <div className="popup-body-company">
                                  {this.state.offer_details.offer_company}&nbsp;
                              </div>
                              <div className="popup-body-city">
                                  - {this.state.offer_details.offer_city} ({this.state.offer_details.offer_cp})
                              </div>
                              <div className="popup-body-type">
                                  {this.state.offer_details.offer_type}
                              </div>
                              <div className="popup-body-description">
                                  {this.state.offer_details.offer_description}
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            }
        </div>
    );
  }
}
