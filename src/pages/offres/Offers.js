import React, {Component} from 'react';
import Popup from 'react-popup';

import {PostData} from "../../services/PostData";

import RowSearch from "./row/RowSearch";
import RowList from "./row/RowList";
import RowPagination from "./row/RowPagination";
import PopupOfferDetails from "./popup/popupOfferDetails";
import PopupNotification from "./popup/PopupNotification";


export default class Offers extends Component {

    constructor(){
        super();

        this.state = {
            search: [],
            offers: [],
            offer_details: [],
            page: 1,
            pages: 1,
            ready: false,
            offerDetails: false,
            user_details: {
              user_id: 1,
              user_name: 'Jeremy Morant',
              user_promotion: 'Master 1'
            },
            university_details: {
              university_id: 1,
              university_name: 'MIAGE',
              university_city: 'Nice Sophia-Antipollis'
            },
            notification: {
              color: '',
              title: '',
              description: ''
            }
        };

        this.loadLatest = this.loadLatest.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.paginationCallBack = this.paginationCallBack.bind(this);
        this.applySentCallback = this.applySentCallback.bind(this);
    }

    componentDidMount(){
        this.loadLatest();
    }

    loadLatest(){
        PostData({api: 'offers-latest', dataSearch: {page: '1'}}).then((result) => {
            let responseJson = result;
            if (responseJson.offersData) {
                this.setState({offers: responseJson.offersData, page: 1, pages: responseJson.pages, ready: true});
                console.log(this.state.offers);
            }else{
                this.setState({
                  offers: [],
                  ready: true,
                  notification: {
                    color: 'red',
                    title: 'Erreur',
                    description: responseJson.error.text
                  }
                });
            }
        });
    }

    researchCallBack = (dataSearch) => {
        this.setState({search: dataSearch, offers: []}, function () {
            let dataSearch = this.state;
            PostData({api: 'offers-search', dataSearch}).then((result) => {
                let responseJson = result;
                if (responseJson.offersData) {
                    this.setState({
                        offers: responseJson.offersData,
                        page: 1,
                        pages: responseJson.pages,
                        redirectToReferrer: true
                    });
                }else{
                    this.setState({
                      offers: [],
                      ready: true,
                      notification: {
                        color: 'red',
                        title: 'Erreur',
                        description: responseJson.error.text
                      }
                    });
                }
            });
        });
    };

    paginationCallBack = (page) => {
        this.setState({page: page, offers: []}, function () {
            let dataSearch = this.state;

            if(Object.keys(this.state.search).length > 0) {
                PostData({api: 'offers-search', dataSearch}).then((result) => {
                    let responseJson = result;
                    if (responseJson.offersData) {
                        this.setState({offers: responseJson.offersData, redirectToReferrer: true});
                    }
                });
            }else{
                PostData({api: 'offers-latest', dataSearch}).then((result) => {
                    let responseJson = result;
                    if (responseJson.offersData) {
                        this.setState({offers: responseJson.offersData, redirectToReferrer: true});
                    }
                });
            }
        });
    };

    applySentCallback = (isSuccess) => {
        if(isSuccess === true){
          this.setState({
            notification: {
              color: 'green',
              title: 'Succès',
              description: "La candidature a bien été envoyée à l'entreprise pour cette offre."
            }
          });
        }else{
          this.setState({
            notification: {
              color: 'red',
              title: 'Erreur',
              description: "Le message n'a pas été envoyé, vous avez déjà répondu à l'offre."
            }
          });
        }
        this.notificationChild.showNotification();
    };

    offerDetailsCallback = (id) => {
            PostData({api: 'offer-details', dataSearch: {id: id}}).then((result) => {
                let responseJson = result;

                if (responseJson.offersData) {

                    let datas = responseJson.offersData;
                    this.setState({offer_details: {
                        offer_id: datas.id,
                        offer_title: datas.title,
                        offer_type: datas.type,
                        offer_company: datas.company_name,
                        offer_description: datas.description,
                        offer_city: datas.nom_reel,
                        offer_cp: datas.code_postal,
                        ready: true
                    }});

                    this.setState({offerDetails: true, offerClicked: id});
                    this.popupChild.showPopup();
                } else {
                    this.setState({
                      offers: [],
                      notification: {
                        color: 'red',
                        title: 'Erreur',
                        description: responseJson.error.text
                      },
                      ready: true});
                      this.notificationChild.showNotification();
                }
            });
    };


    render() {
        return (
            <div className="Offers">
                <RowSearch researchCallBack={this.researchCallBack}/>

                { this.state.ready &&
                    <div>
                        <RowList offersDatas={this.state.offers} offerDetailsCallback={this.offerDetailsCallback} />
                        <RowPagination paginationCallBack={this.paginationCallBack} offersPage={this.state.page} offersPages={this.state.pages} />
                    </div>
                }
                { this.state.offerDetails &&
                    <PopupOfferDetails  ref={instance => { this.popupChild = instance; }} applySentCallback={this.applySentCallback}  userDetails={this.state.user_details} universityDetails={this.state.university_details}  offerDetails={this.state.offer_details} />
                }

                <PopupNotification ref={instance => { this.notificationChild = instance; }} color={this.state.notification.color} title={this.state.notification.title} description={this.state.notification.description} />
            </div>
        );
    }

}
