import React, {Component} from 'react';

import {PostData} from '../../services/PostData';
import './LoginPage.css';

import FaUser from 'react-icons/lib/fa/user';
import FaLogout from 'react-icons/lib/fa/sign-out';

import PopupNotification from './popup/PopupNotification';


export default class LoginPage extends Component {
    constructor(){
        super();
        this.state = {
          api: 'login',
          email: '',
          password: '',
          redirectToReferrer: false,
          loginActive: '',
          notification: {
            color: '',
            title: '',
            description: ''
          }
        };
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.loginShow = this.loginShow.bind(this);
        this.loginHide = this.loginHide.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    loginShow(){
        this.setState({loginActive: "active"});
    }

    loginHide(){
        this.setState({loginActive: ""});
    }

    login() {
        if(this.state.email && this.state.password){
            PostData(this.state).then((result) => {
                let responseJson = result;
                if(responseJson.userData){
                    sessionStorage.setItem('userData',JSON.stringify(responseJson.userData));
                    window.location.reload();
                }else{
                  this.setState({ notification : {
                    color: 'red',
                    title: 'Erreur',
                    description: responseJson.error.text
                  }});
                  this.notificationChild.showNotification();
                }
            });
        }
    }

    logout() {
        sessionStorage.clear();
        this.setState({redirectToReferrer: true});
    }

    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }

    render() {
        return (
                <div className="login-container-page">
                    <div className="login-inner">
                        <p className="login-title-big">Accès Membre</p>
                        <p className="login-title">Vous devez être connecté pour accéder à ce contenu, si vous n&apos;avez toujours pas de compte <a href="/register">demandez-en un ici</a></p>
                        <div className="field">
                            <input id="login-page-email" type="text" name="email" placeholder=" " onChange={this.onChange}/>
                            <label htmlFor="login-email">Adresse email</label>
                        </div>
                        <div className="field">
                            <input id="login-page-password" type="password" name="password" placeholder=" " onChange={this.onChange}/>
                            <label htmlFor="login-password">Mot de passe</label>
                        </div>

                        <input type="submit" value="Valider" onClick={this.login}/>

                        <div className="links">
                            <p>Mot de passe oublié ?</p>
                        </div>
                    </div>
                    <PopupNotification ref={instance => { this.notificationChild = instance; }} color={this.state.notification.color} title={this.state.notification.title} description={this.state.notification.description} />

                </div>
        );
    }


}
