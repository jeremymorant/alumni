import React from 'react';

import './UserDetails.css';
export default class UserDetails extends React.Component {
  render() {

    return (
      <div class="container">
        <br/>
        <div class="row">
          {/***TOPBAR***/}
          <div class="col-lg-12 profileHeader">
            <br/>
            <div class="profileHeaderBackground">
              <img src="http://via.placeholder.com/1110x250" alt=""/>
            </div>
            <div class="profileSummary">
              <div class="col-lg-6 profilePicture">
                <div class="col-6"><img src="https://bootdey.com/img/Content/user_1.jpg" alt=""/></div>
                <div class="col-6 text-left profileInfo">
                  <h2>Luke Bancroft-Richardson</h2>
                  <h3>Alumni</h3>
                  <button type="button" class="btn btn-primary">Follow</button>
                  <button type="button" class="btn btn-success">Send message</button>
                </div>
              </div>
              <div class="col-lg-6 text-right profileStats">
                15 Followers
              </div>
              <br/>
            </div>
          </div>
          {/***LEFT BAR***/}
          <div class="col-lg-3 text-center">
            <br/>
            <p>leftbar</p>
            <br/>
          </div>
          {/***MIDDLE CONTENT***/}
          <div class="col-lg-6 text-center">
            <br/>
            <p>content</p>
            <br/>
          </div>
          {/***RIGHT BAR***/}
          <div class="col-lg-3 text-center">
            <br/>
            <p>rightbar</p>
            <br/>
          </div>
        </div>
      </div>
    );
  }
}
