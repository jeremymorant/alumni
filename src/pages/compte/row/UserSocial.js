import React from 'react';
import { PostData } from '../../../services/PostData';


import './UserSocial.css';

import FaFacebook from 'react-icons/lib/fa/facebook';
import FaTwitter from 'react-icons/lib/fa/twitter';
import FaLinkedin from 'react-icons/lib/fa/linkedin';
import FaTrash from 'react-icons/lib/fa/trash';
import FaCross from 'react-icons/lib/fa/times-circle';
import FaEdit from 'react-icons/lib/fa/edit';

export default class UserInformations extends React.Component {

  constructor(){
    super();

    this.state = {
      popupSocial: false,
      socialClicked: '',
      socialClickedUrl: ''
    }
  }

  clickSocial = (social) => {
    this.setState({
      popupSocial: true,
      socialClicked: social
    });
  }

  removeSocial = (social) => {
    let sessionUser = sessionStorage.getItem('userData');
        sessionUser =  JSON.parse(sessionUser);

    PostData({
      api: 'account-social-remove',
      dataUser: {
        social: social,
        user_id: sessionUser.id
      }
    }).then((result) => {
      this.props.callBackSocial(sessionUser.id);
      this.hidePopup();
    });
  }

  editSocial = () => {
    let sessionUser = sessionStorage.getItem('userData');
        sessionUser =  JSON.parse(sessionUser);

    PostData({
      api: 'account-social-edit',
      dataUser: {
        social: this.state.socialClicked,
        url: this.state.socialClickedUrl,
        user_id: sessionUser.id
      }
    }).then((result) => {
      this.props.callBackSocial(sessionUser.id);
      this.hidePopup();
    });
  }

  onChangeLink = (e) => {
    this.setState({socialClickedUrl: e.target.value});
  }

  hidePopup = () => {
      this.setState({popupSocial: false, socialClicked: '', socialClickedUrl: ''});
  }

  render() {
    let arrayLink = [];
    let facebookLink = '...';
    let twitterLink = '...';
    let linkedinLink = '...';
    if(this.props.facebookDatas !== undefined) arrayLink = this.props.facebookDatas.split('.com/');
    if(arrayLink.length > 1){
      facebookLink = arrayLink[1].trim();
    }
    arrayLink = [];
    if(this.props.linkedinDatas !== undefined) arrayLink = this.props.linkedinDatas.split('.com/');
    if(arrayLink.length > 1){
      linkedinLink = arrayLink[1].trim();
    }
    arrayLink = [];
    if(this.props.twitterDatas !== undefined) arrayLink = this.props.twitterDatas.split('.com/');
    if(arrayLink.length > 1){
      twitterLink = arrayLink[1].trim();
    }
    return (
        <div className="user-social">
          <div className="user-social-inner">
              <div className="user-social-title">Mes réseaux sociaux</div>
              <ul className="user-social-ul">
                <li>
                  <a className="hexagon" href={ (facebookLink.length > 3) ? this.props.facebookDatas : "javascript:void(0);"} target={ (facebookLink.length > 3) ? "_blank" : "" }><FaFacebook /></a>
                  <p className="user-social-text-link">{ facebookLink }</p>
                  <span className="user-social-edit" onClick={() => this.clickSocial("facebook")}><FaEdit /></span>
                  <span className="user-social-trash" onClick={() => this.removeSocial("facebook")}><FaTrash /></span>
                </li>
                <li>
                  <a className="hexagon" href={ (twitterLink.length > 3) ? this.props.twitterDatas : "javascript:void(0);"} target={ (twitterLink.length > 3) ? "_blank" : "" }><FaTwitter /></a>
                  <p className="user-social-text-link">{ twitterLink }</p>
                  <span className="user-social-edit" onClick={() => this.clickSocial("twitter")}><FaEdit /></span>
                  <span className="user-social-trash" onClick={() => this.removeSocial("twitter")}><FaTrash /></span>
                </li>
                <li>
                  <a className="hexagon" href={ (linkedinLink.length > 3) ? this.props.linkedinDatas : "javascript:void(0);"} target={ (linkedinLink.length > 3) ? "_blank" : "" }><FaLinkedin /></a>
                  <p className="user-social-text-link">{ linkedinLink }</p>
                  <span className="user-social-edit" onClick={() => this.clickSocial("linkedin")}><FaEdit /></span>
                  <span className="user-social-trash" onClick={() => this.removeSocial("linkedin")}><FaTrash /></span>
                </li>
              </ul>
          </div>

          <div className={this.state.popupSocial ? 'popup' : 'popup hidden'}>
            <div className="popup-overlay" onClick={this.hidePopup}> </div>
            <div className="popup-container">
              <div className="popup-header">
                  <span className="popup-header-title">Mon réseau {this.state.socialClicked}</span>
                  <FaCross className="popup-cross" onClick={this.hidePopup}/>
              </div>
              <div className="popup-body">
                <div className="field">
                    <input id="popup-social-url" type="text" name="socialUrl" placeholder=" " onChange={this.onChangeLink}/>
                    <label htmlFor="popup-social-url">Lien de ma page {this.state.socialClicked}</label>
                </div>
                <div className="button" onClick={this.editSocial}>Modifier le lien</div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
