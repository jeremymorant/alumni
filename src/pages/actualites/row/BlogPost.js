import React from 'react';

import {PostData} from "../../../services/PostData";


export default class BlogPost extends React.Component {
  constructor(){
      super();
      this.state = { api: '', id: '', title: '', author: '', date: '', image: '', content: '', comments: [], redirectToReferrer: false};
      this.getPost = this.getPost.bind(this);
      this.getComments = this.getComments.bind(this);
      this.componentDidMount = this.componentDidMount.bind(this);
  }

  getPost() {
    this.setState({ api: 'blog-post', id: this.props.post_id }, () => {
      PostData(this.state).then((result) => {
        let responseJson = result;
        if(responseJson.postDetails){
            this.setState({
              title: responseJson.postDetails.title,
              author_name: responseJson.postDetails.author_name,
              author_id: responseJson.postDetails.author_id,
              date: responseJson.postDetails.date,
              image: responseJson.postDetails.image,
              content: responseJson.postDetails.content,
              redirectToReferrer: true});
        }
        this.getComments();
      });
    });
  }

  getComments() {
    this.setState({ api: 'blog-post-comments'}, () => {
      PostData(this.state).then((result) => {
        let responseJson = result;
        if(responseJson.commentDetails){
            this.setState({
              comments: responseJson.commentDetails});
        }
      });
    });
  }

  componentDidMount(){
      this.getPost();
  }

  render() {
    return (
      <div className="container">

      <div className="row">
      <div className="col-lg-8">

        {/*Title*/}
        <h1 className="mt-4">{this.state.title}</h1>

        {/*Author*/}
        <p className="lead">
          par
          <a href={'/annuaire?user_id='+this.state.author_id}> {this.state.author_name}</a>
        </p>

        <hr/>

        {/*Date/Time*/}
        <p>Posté le {this.state.date}</p>

        <hr/>

        {/*Preview Image*/}
        <img className="img-fluid rounded" src={this.state.image} alt=""/>

        <hr/>

        {/*Post Content*/}
        <div dangerouslySetInnerHTML={{ __html: this.state.content }} ></div>

        <hr/>

        {/*Comments Form*/}
        <div className="card my-4">
          <h5 className="card-header">Laisser un commentaire:</h5>
          <div className="card-body">
            <form>
              <div className="form-group">
                <textarea className="form-control" rows="3"></textarea>
              </div>
              <button type="submit" className="btn btn-primary">Envoyer</button>
            </form>
          </div>
        </div>
        {
          this.state.comments.map(el =>
            <div key={'user-block-id-'+el.id} className="media mb-4">
              <img className="d-flex mr-3 rounded-circle" src="http://via.placeholder.com/50x50" alt=""/>
              <div ckey={'comment-id-'+el.id} className="media-body">
                <h5 className="mt-0">{el.fullname}</h5>
                {el.comment}
              </div>
            </div>
          )
        }

      </div>
      <div className="col-md-4">
        {/*Search Widget*/}
        <div className="card my-4">
          <h5 className="card-header">Recherche</h5>
          <div className="card-body">
            <div className="input-group">
              <input type="text" className="form-control" placeholder="..."/>
              <span className="input-group-btn">
                <button className="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>

        {/*Categories Widget*/}
        <div className="card my-4">
          <h5 className="card-header">Archives</h5>
          <div className="card-body">
            <div className="row">
              <div className="col-lg-6">
                <ul className="list-unstyled mb-0">
                  <li>
                    <a href="#">Janvier 2018</a>
                  </li>
                  <li>
                    <a href="#">Fevrier 2018</a>
                  </li>
                  <li>
                    <a href="#">Mars 2018</a>
                  </li>
                </ul>
              </div>
              <div className="col-lg-6">
                <ul className="list-unstyled mb-0">
                  <li>
                    <a href="#">Avril 2018</a>
                  </li>
                  <li>
                    <a href="#">Mai 2018</a>
                  </li>
                  <li>
                    <a href="#">Juin 2018</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        {/*Side Widget*/}
        <div className="card my-4">
          <h5 className="card-header">Twitter</h5>
          <div className="card-body">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?
          </div>
        </div>

      </div>
      </div>
      </div>
    );
  }
}
