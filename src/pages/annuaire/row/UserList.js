import React from 'react';

import './UserList.css';
import {PostData} from "../../../services/PostData";

export default class UserList extends React.Component {
  constructor(){
      super();
      this.state = { api: 'directory-users', users: [], searchTerm: '', redirectToReferrer: false};
      this.getUsers = this.getUsers.bind(this);
      this.search = this.search.bind(this);
      this.componentDidMount = this.componentDidMount.bind(this);
  }

  getUsers() {
    PostData(this.state).then((result) => {
        let responseJson = result;
        if(responseJson.usersDetails){
            this.setState({
              users: responseJson.usersDetails,
              redirectToReferrer: true});
        }
    });
  }

  search(searchTerm) {
    this.setState({searchTerm: searchTerm, users: []}, () => {
      this.getUsers();
    });
  }

  componentDidMount(){
      this.getUsers();
  }

  render() {
    return (
      <div className="container">
        <br/>

        <div class="row">
          <div className="search-bar-title">
              <h1>Annuaire des Miagistes</h1>
              <p>Parcourez la liste de tous les alumnis et étudiants pour en voir leurs détails, les contactez ou par simple curiosité ! Des filtres et une barre de recherche sont également à votre disposition dans cet annuaire</p>
          </div>
          <br/>
          <div class="col-lg-8">

          </div>
          <div class="col-lg-4 float-right">
            <div className="input-group">
              <input type="text" className="form-control" id="userSearch" placeholder="Rechercher des utilisateurs..."/>
              <span className="input-group-btn">
                <button className="btn btn-secondary" onClick={(e) => this.search(document.getElementById('userSearch').value)} type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>

        <div class="row">
        <div class="col-lg-12">
            <div class="main-box no-header clearfix">
                <div class="main-box-body clearfix">
                    <div class="table-responsive">
                        <table class="table user-list">
                            <thead>
                                <tr>
                                <th><span>Membre</span></th>
                                <th class="text-center"><span>Promotion</span></th>
                                <th><span>Email</span></th>
                                <th class="text-center">Réseaux sociaux</th>
                                </tr>
                            </thead>
                            <tbody>
                            {
                              this.state.users.map(el =>
                                <tr key={'user-id-'+el.id} >
                                    <td>
                                        <img src="https://bootdey.com/img/Content/user_1.jpg" alt=""/>
                                        <a key={'user-name-id-'+el.id} href={'/annuaire?user_id=' + el.id} class="user-link">{el.fullName}</a>
                                        <span class="user-subhead">{el.status}</span>
                                    </td>
                                    <td key={'user-promotion-id-'+el.id} class="text-center">{el.promotion}</td>
                                    <td className="align-middle">
                                        <a key={'user-email-id-'+el.id} href="">{el.email}</a>
                                    </td>
                                    <td className="smallerCell text-center">
                                      {el.linkedin ?
                                        <a key={'user-linkedin-id-'+el.id} href={el.linkedin} class="table-link">
                                            <span class="fa-stack">
                                                <i class="fa fa-square fa-stack-2x"></i>
                                                <i class="fab fa-linkedin-in fa-stack-1x fa-inverse"></i>
                                            </span>
                                      </a> : null}
                                      {el.twitter ?
                                        <a key={'user-twitter-id-'+el.id} href={el.twitter} class="table-link">
                                            <span class="fa-stack">
                                                <i class="fa fa-square fa-stack-2x"></i>
                                                <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                                            </span>
                                      </a> : null}
                                      {el.gplus ?
                                        <a key={'user-gplus-id-'+el.id} href={el.gplus} class="table-link danger">
                                            <span class="fa-stack">
                                                <i class="fa fa-square fa-stack-2x"></i>
                                                <i class="fab fa-google-plus fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </a> : null}
                                        <a href="#" class="table-link">
                                            <span class="fa-stack">
                                                <i class="fa fa-square fa-stack-2x text-warning"></i>
                                                <i class="fas fa-envelope fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </a>
                                    </td>
                                </tr>
                                )
                              }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
      </div>
    );
  }
}
