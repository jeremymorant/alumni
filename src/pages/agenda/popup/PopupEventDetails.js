import React, {Component} from 'react';

import './PopupEventDetails.css';
import {PostData} from "../../../services/PostData";

import FaCross from 'react-icons/lib/fa/times-circle';

export default class PopupeventDatas extends Component {
    constructor(props){
        super(props);
        this.state = {
            ready: false,
            form: false,
            message: ''
        };
        this.hidePopup = this.hidePopup.bind(this);
        this.showPopup = this.showPopup.bind(this);
        this.cancelForm = this.cancelForm.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.showForm = this.showForm.bind(this);
        this.onChange = this.onChange.bind(this);
    }


    hidePopup(){
        console.log("hidePopup");
        this.setState({ready: false, form: false});
        //clear fields
    }

    showPopup(){
        console.log("showPopup");
        this.setState({ready: true});
    }

    showForm(){
        console.log("showForm");
        this.setState({form: true});
    }

    cancelForm(){
      console.log("cancelForm");
      //
      //
      //
      this.setState({form: false});
    }

    onChange(e){
          this.setState({[e.target.name]:e.target.value});
    }

    render() {

        return (
                  <div className={this.state.ready ? 'popup' : 'popup hidden'}>
                      <div className="popup-overlay" onClick={this.hidePopup}> </div>
                      <div className="popup-container">
                          { !this.state.form &&
                              <div className="popup-details">
                                  <div className="popup-header">
                                      <span className="popup-header-title">Détails de l&apos;événement</span>
                                      <FaCross className="popup-cross" onClick={this.hidePopup}/>
                                  </div>
                                  <div className="popup-body">
                                      <div className="popup-body-title">
                                            {this.props.eventDatas.title}
                                      </div>
                                      <div className="popup-body-type">
                                            {this.props.eventDatas.type}
                                      </div>
                                      <div className="popup-body-address">
                                            {this.props.eventDatas.address} - { this.props.eventDatas.nom_reel}
                                      </div>
                                      <div className="popup-body-date">
                                            A partir du {this.props.eventDatas.startDate}
                                      </div>
                                      <div className="popup-body-date">
                                            Jusqu&apos;au { this.props.eventDatas.endDate}
                                      </div>
                                      <div className="popup-body-description">
                                            {this.props.eventDatas.description}
                                      </div>
                                  </div>
                              </div>
                          }
                      </div>
                  </div>
        );
    }

    validateForm(){
      if(this.state.message.length > 0){

        let datas = {
            user_id: this.props.userDetails.user_id,
            user_name: this.props.userDetails.user_name,
            university_name: this.props.universityDetails.university_name,
            university_city: this.props.universityDetails.university_city,
            user_promotion: this.props.userDetails.user_promotion,
            offer_id: this.props.eventDatas.offer_id,
            offer_type: this.props.eventDatas.offer_type,
            offer_title: this.props.eventDatas.offer_title,
            email_message: this.state.message
        }
        PostData({api: 'offer-apply', dataApply: datas}).then((result) => {
            let responseJson = result;

            if (responseJson.offersData) {

                let datas = responseJson.offersData;
                this.hidePopup();

            } else {
                //this.setState({offers: [], ready: true, redirectToReferrer: true});
            }
        });
      }
    }
}
