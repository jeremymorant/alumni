import React, {Component} from 'react';

import './RowList.css';

export default class RowList extends Component {

    render() {
        return (
            <div className="offers-list">
                {
                    this.props.offersDatas.map(el =>
                        <div className="offer-item" onClick={() => this.props.offerDetailsCallback(el.id)} key={'offer-'+el.id}>
                            <div className="offer-item-left">
                                <div className="offer-type" key={'offer-type-'+el.id}>{el.type}</div>
                            </div>
                            <div className="offer-item-right">
                                <div className="offer-title" key={'offer-title-'+el.id}>{el.title}</div>
                                <div className="offer-company" key={'offer-company-'+el.id}>Offre de : {el.company_name}</div>
                                <div className="offer-description" key={'offer-description-'+el.id}>{el.description}</div>
                            </div>
                        </div>
                    )
                }
            </div>
        )
    }
}