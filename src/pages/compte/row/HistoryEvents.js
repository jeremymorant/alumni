import React from 'react';

import './HistoryEvents.css';

export default class HistoryEvents extends React.Component {
  render() {

    return (
        <div className="history-events">
            <div className="history-events-inner">
                <div className="history-events-title">
                    Derniers évènements intéressants
                </div>
                <div className="events-item-list">
                {
                    this.props.eventsDatas.map(el =>
                        <div className="event-item" onClick={() => this.props.eventDetailsCallback(el.id)} key={'event-'+el.id}>
                            <div className="event-item-left">
                                <div className="event-type" key={'event-type-'+el.id}>{el.type}</div>
                            </div>
                            <div className="event-item-right">
                                <div className="event-title" key={'event-title-'+el.id}>{el.title}</div>
                                <div className="event-company" key={'event-company-'+el.id}>Offre de : {el.company_name}</div>
                                <div className="event-description" key={'event-description-'+el.id}>{el.description}</div>
                            </div>
                        </div>
                    )
                }
                </div>
            </div>
        </div>
    );
  }
}
