import React, {Component} from 'react';

import logo from './logo.png';
import logoBis from './logo-bis.png';
import './Header.css';

export default class Header extends Component {

    menuItems = ["home","actualites", "agenda", "annuaire","offres"];

    render() {
        return (
            <header className="App-header">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossOrigin="anonymous"/>
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossOrigin="anonymous"/>
                <div className="App-header-container">
                    <div id="App-logo-container">
                        <img src={logo} className="App-logo" alt="logo" />
                        <span className="App-title">
                          <h1>MIAGE ALUMNI</h1>
                          <h2>Nice - Sophia-Antipolis</h2>
                        </span>
                        <img src={logoBis} className="App-logo-bis" alt="logo" />
                    </div>
                    <div id="App-menu-container">
                        <ul>
                            {
                                this.menuItems.map(el => <li key={'menu-'+el}><a key={'menu-a-'+el} href={el}> {el}</a></li>)
                            }
                        </ul>
                    </div>
                    <div id="App-burger-container" onClick={this.showBurger}>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div id="App-menu-burger-container">
                    <ul>
                        {
                            this.menuItems.map(el => <li key={'menu-burger-'+el} ><a key={'menu-burger-a'+el} href={el}> {el}</a></li>)
                        }
                    </ul>
                </div>
            </header>
        );
    }

    showBurger(e) {
        e.preventDefault();
        var target = e.target;

        if(target.className  === "active"){
            target.className = "";
            document.getElementById('App-menu-burger-container').className = "";
            document.getElementById('App-logo-container').className = "";
            let logged = document.getElementById('login-container-logged');
            if(logged) logged.className = "";
        }else{
            target.className += "active";
            document.getElementById('App-menu-burger-container').className += "active";
            document.getElementById('App-logo-container').className += "active";
            let logged = document.getElementById('login-container-logged');
            if(logged) logged.className = "active";
        }
    }

    hideBurger(e){

    }

}
