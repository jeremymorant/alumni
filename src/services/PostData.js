export function PostData(userData) {

  let BaseURL = 'https://jeremy-morant.000webhostapp.com/index.php';

  return new Promise((resolve, reject) =>{
    fetch(BaseURL, {
      method: 'POST',
      headers: {
        'content-type': 'multipart/form-data'
      },
      body: JSON.stringify(userData)
    })
    .then((response) => response.json())
    .then((res) => {
      resolve(res);
    })
    .catch((error) => {
      reject(error);
    });
  });
}
