import React from 'react';

import './Companies.css';
import FaUser from 'react-icons/lib/fa/building';
import FaEdit from 'react-icons/lib/fa/edit';
import FaAdd from 'react-icons/lib/fa/plus';
import FaRemove from 'react-icons/lib/fa/trash';

export default class Companies extends React.Component {

  render() {
    return (
        <div className="admin-row">
          <div className="title-icon">
            <FaUser />
          </div>
          <div className="title">
            Entreprises
          </div>
          <div className="actions">
            <div className="action-item add"><FaAdd /></div>
            <div className="action-item edit"><FaEdit /></div>
            <div className="action-item remove"><FaRemove /></div>
          </div>
        </div>
    );
  }
}
