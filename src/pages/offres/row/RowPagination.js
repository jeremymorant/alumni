import React from 'react';

import './RowPagination.css';

export default class RowPagination extends React.Component {

    render() {
        return (
            <div className="pagination-container">
                { this.props.offersPage === this.props.offersPages && this.props.offersPages > 4 &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage - 4)}>{this.props.offersPage - 4 }</div>
                }
                { this.props.offersPage === this.props.offersPages && this.props.offersPages > 4 &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage - 3)}>{this.props.offersPage - 3}</div>
                }
                { this.props.offersPage === this.props.offersPages - 1 && this.props.offersPages > 4 &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage - 3)}>{this.props.offersPage - 3  }</div>
                }


                { this.props.offersPage > 2 &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage - 2)}>{this.props.offersPage - 2}</div>
                }
                { this.props.offersPage > 1 &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage - 1)}>{this.props.offersPage - 1}</div>
                }


                <div className="pagination-item active">{this.props.offersPage}</div>



                { this.props.offersPage + 1 <= this.props.offersPages &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage + 1)}>{this.props.offersPage + 1}</div>
                }
                {  this.props.offersPage + 2 <= this.props.offersPages &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage + 2)}>{this.props.offersPage + 2}</div>
                }


                { this.props.offersPage === 1 && this.props.offersPages > 3 &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage + 3)}>{this.props.offersPage + 3}</div>
                }
                {  this.props.offersPage === 1 && this.props.offersPages > 4 &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage + 4)}>{this.props.offersPage + 4}</div>
                }
                {  this.props.offersPage === 2 && this.props.offersPages > 4 &&
                <div className="pagination-item" onClick={() => this.props.paginationCallBack(this.props.offersPage + 3)}>{this.props.offersPage + 3}</div>
                }
            </div>
        )
    }
}