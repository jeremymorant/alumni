import React, {Component} from 'react';

import Slider from "./slider/Slider";
import RowSummary from "./row/RowSummary";
import RowStats from "./row/RowStats";
import RowNews from "./row/RowNews";

export default class Home extends Component {


  componentDidMount() {
    // bug on carousel module
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 100);
  }

  render() {
    return (
      <div className="Home">
        <Slider />
        <RowStats />
        <RowSummary />
        <RowNews />
      </div>
    );
  }


}
