import React, {Component} from 'react';

import FaFacebook from 'react-icons/lib/fa/facebook';
import FaTwitter from 'react-icons/lib/fa/twitter';
import FaLinkedin from 'react-icons/lib/fa/linkedin';

import gfi from './gfi.png';
import './Footer.css';

export default class Footer extends Component {


    menuItems = ["home","famille", "actualites", "agenda", "annuaire","offres","reseaux"];

    sponsorsItems = [gfi];

    render() {
        return (
            <footer className="App-footer">
                <div className="App-footer-container">
                    <div className="App-footer-item">
                        <ul className="App-footer-menu">
                            {
                                this.menuItems.map(el => <li  key={'footer-'+el} ><a key={'footer-a-'+el} href={el}> {el} </a></li>)
                            }
                        </ul>
                    </div>
                    <div className="App-footer-item">
                        <ul className="App-footer-social">
                            <li><a className="hexagon" href="https://www.facebook.com"><FaFacebook /></a></li>
                            <li><a className="hexagon" href="https://www.twitter.com"><FaTwitter /></a></li>
                            <li><a className="hexagon" href="https://www.linkedin.com"><FaLinkedin /></a></li>
                        </ul>
                        <div className="App-footer-register">
                            <p className="App-footer-register-title">Demandez votre compte</p>
                            <p className="App-footer-register-subtitle">Pour accéder aux informations, vous devez d&apos;abord vous inscrire</p>
                            <input type="email" className="App-footer-register-input" placeholder="Votre email"/>
                            <input type="submit" className="App-footer-register-submit" value=">"/>
                        </div>
                    </div>
                    <div className="App-footer-item">
                        <div className="App-footer-sponsors-title">
                            Entreprises partenaires
                        </div>
                        <ul className="App-footer-sponsors">
                            {
                                this.sponsorsItems.map(el => <li key={'footer-sponsors-'+el}><img alt={'entreprise '+el} key={'footer-sponsors-img-'+el} src={el} /></li>)
                            }
                        </ul>
                    </div>
                </div>
                <div className="App-footer-container-bottom">
                    <div className="App-footer-bottom-right">
                        Politique de confidentialité
                    </div>
                    <div className="App-footer-bottom-left">
                        2018 &copy; Miage Alumni Nice - Sophia-Antipolis
                    </div>
                </div>
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossOrigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossOrigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossOrigin="anonymous"></script>
            </footer>
        );
    }

}
