import React from 'react';

import './RowNews.css';
import {PostData} from "../../../services/PostData";
import Twitter from './img/twitter.png';

export default class extends React.Component {
  constructor(){
      super();
      this.state = { api: 'home-news', news: [], redirectToReferrer: false};
      this.getNews = this.getNews.bind(this);
      this.getSubtitle = this.getSubtitle.bind(this);
      this.componentDidMount = this.componentDidMount.bind(this);
  }

  getNews() {
    PostData(this.state).then((result) => {
        let responseJson = result;
        if(responseJson.news){
            this.setState({
              news: responseJson.news,
              redirectToReferrer: true});
        }
    });
  }

  getSubtitle(str) {
    return str.substring(str.indexOf(">") + 1, str.indexOf("</p>"));
  }

  componentDidMount(){
      this.getNews();
  }

    render() {
        return (
            <div>
                <h1 className="RowNews-title">Les Dernières Actualités</h1>
                <div className="RowNews-container">
                  {
                    this.state.news.map(el =>
                      <a href={"/actualites?post_id=" + el.id}>
                        <div className="RowNews-item RowNews-item-big" key={'news-item-'+el.id}>
                            <div className="RowNews-button">
                                <div className="RowNews-button-icon" key={'news-thumbnail-'+el.id}>
                                    <img alt={el.title} src={el.image} />
                                </div>
                                <div className="RowNews-button-title" key={'news-title-'+el.id}>
                                    {el.title}
                                </div>
                                <div className="RowNews-button-subtitle"
                                  dangerouslySetInnerHTML={{ __html: this.getSubtitle(el.content)}}
                                  key={'news-subtitle-'+el.id}>
                                </div>
                            </div>
                        </div>
                      </a>
                      )
                    }
                    <div className="RowNews-item RowNews-item-small">
                        <img src={Twitter} alt="twitter" />
                    </div>
                </div>
            </div>
        );
    }
}
